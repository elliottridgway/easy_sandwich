## EasySandwich

In 2014, I was commissioned to write simple slideshow software for a local sandwich shoppe business.  This software allows the user to add images, write text that appears over those images, and schedule the images and text to display as a slideshow.  There were a few other simple features that were requested, like displaying the date.
 
This software can work as general slideshow software, but it comes with an example "sandwich menu" deck of slides, with example images that were made specifically for the sandwich shoppe who commissioned the software.
 
Written in C++ using Win32 and the SDL library.

---

**Using the Software**

Run EasySandwich.exe (from the release branch) to run the software.

The config.txt file contains some values you can edit to adjust how the slideshow displays.  For instance, the slideshow demo created for the sandwich shoppe assumed a screen resolution of 1366 x 768.  You can change "ScreenWidth" and "ScreenHeight" in config.txt to change what resolution the slideshow displays at.

---

**The Branches**

The "master" branch contains all the uncompiled source code for EasySandwich.

The "release" branch has compiled builds of the software.