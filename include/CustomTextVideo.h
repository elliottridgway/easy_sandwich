#ifndef CUSTOMTEXTVIDEO_H
#define CUSTOMTEXTVIDEO_H

#include <string>
#include "resource.h"

class Video {
    public:
        Video();
        virtual ~Video();
        void setVideoType(int val);
        std::string Getfilepath();
        void Setfilepath(std::string val);
        int getVideoType();
        virtual std::string print() {
            return "ERROR";
        }
    private:
        int type;
    protected:
        std::string filepath;
};

class TemperatureVideo: public Video
{
    public:
        TemperatureVideo();
        virtual ~TemperatureVideo();
        std::string Gettemperature();
        void Settemperature(std::string val);
        std::string print();
    private:
        std::string temperature;
};

class DateVideo: public Video
{
    public:
        DateVideo();
        virtual ~DateVideo();
        bool Getday();
        void Setday(bool val);
        bool Getmonth();
        void Setmonth(bool val);
        bool Getyear();
        void Setyear(bool val);
        std::string print();
    private:
        bool day;
        bool month;
        bool year;
};

class CustomTextVideo: public Video
{
    public:
        CustomTextVideo();
        CustomTextVideo(Video&);
        virtual ~CustomTextVideo();
        std::string Gettitle();
        void Settitle(std::string val);
        std::string Gettext1();
        void Settext1(std::string val);
        std::string Gettext2();
        void Settext2(std::string val);
        std::string Gettext3();
        void Settext3(std::string val);
        std::string Gettext4();
        void Settext4(std::string val);
        std::string Gettext5();
        void Settext5(std::string val);
        std::string Gettext6();
        void Settext6(std::string val);
        std::string Gettext7();
        void Settext7(std::string val);
        std::string Gettext8();
        void Settext8(std::string val);
        std::string Gettext9();
        void Settext9(std::string val);
        std::string Gettext10();
        void Settext10(std::string val);
        int GetappearDirection();
        void SetappearDirection(int val);
        int GetalignDirection();
        void SetalignDirection(int val);
        void setVideoType(int val);
        int getVideoType();
        std::string print();
    private:
        std::string title;
        std::string text1;
        std::string text2;
        std::string text3;
        std::string text4;
        std::string text5;
        std::string text6;
        std::string text7;
        std::string text8;
        std::string text9;
        std::string text10;
        int appearDirection;
        int alignDirection;
};

#endif // CUSTOMTEXTVIDEO_H
