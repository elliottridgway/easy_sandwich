#include "main.h"

using namespace std;

const char g_szClassName[] = "myWindowClass";
HWND g_mainDialog = NULL;
CustomTextVideo globalVideo;
TemperatureVideo globalTemperatureVideo;
DateVideo globalDateVideo;
vector<unique_ptr<Video>> videoSchedule;
bool editMode = false;
bool randomOrder = false;

const int MAX_CHARS_PER_LINE = 2500;
const int MAX_TOKENS_PER_LINE = 100;
const char* const DELIMITER = " ";

char* openFile(HWND hwnd) {
    // Open file dialo g
    OPENFILENAME ofn;
    char szFile[260];       // buffer for file name
    HANDLE hf;              // file handle

    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwnd;
    ofn.lpstrFile = szFile;
    // Set lpstrFile[0] to '\0' so that GetOpenFileName does not
    // use the contents of szFile to initialize itself.
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (GetOpenFileName(&ofn)==TRUE) {
        hf = CreateFile(ofn.lpstrFile,
                        GENERIC_READ,
                        0,
                        (LPSECURITY_ATTRIBUTES) NULL,
                        OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL,
                        (HANDLE) NULL);
    }
    string fileName(ofn.lpstrFile);
    CloseHandle(hf);
    return (char*)fileName.c_str();
}

int saveVideoScheduleToFile()
{
    ofstream file;
    file.open("schedule.txt", ios::out | ios::trunc);
    for (int i = 0; i < (int)videoSchedule.size(); i++) {
        file << videoSchedule[i]->print() << "\n";
    }
    file.close();
    return 0;
}

int loadConfigFile(HWND hwnd) {
    // create a file-reading object
    ifstream fin;
    fin.open("config.txt"); // open a file
    if (!fin.good())
        return 1; // exit if file not found

    // read each line of the file
    while (!fin.eof())
    {
        // read an entire line into memory
        char buf[MAX_CHARS_PER_LINE];
        fin.getline(buf, MAX_CHARS_PER_LINE);

        // parse the line into double-colon delimited tokens
        int n = 0; // a for-loop index

        // array to store memory addresses of the tokens in buf
        const char* token[MAX_TOKENS_PER_LINE] = {}; // initialize to 0

        // parse the line
        token[0] = strtok(buf, DELIMITER); // first token
        if (token[0]) // zero if line is blank
        {
            for (n = 1; n < MAX_TOKENS_PER_LINE; n++) {
                token[n] = strtok(0, DELIMITER); // subsequent tokens
                if (!token[n]) break; // no more tokens
            }

            // Process tokens
            if (strcmp(token[0], "ScreenWidth") == 0) {
                const char* width = (char*)token[1];
                SCREEN_WIDTH = atoi(width);
            }
            if (strcmp(token[0], "ScreenHeight") == 0) {
                const char* height = (char*)token[1];
                SCREEN_HEIGHT = atoi(height);
            }
            if (strcmp(token[0], "FontTitle") == 0) {
                titleFontPath = token[1];
            }
            if (strcmp(token[0], "FontMain") == 0) {
                mainFontPath = token[1];
            }
            if (strcmp(token[0], "RandomOrder") == 0) {
                if (strcmp(token[1], "True") == 0) randomOrder = true;
            }
            if (strcmp(token[0], "BaseTime") == 0) {
                const char* baseTime = (char*)token[1];
                BASE_SCENE_TIME = atoi(baseTime);
            }
        }
    }
    return 0;
}

int loadVideoScheduleToFile(HWND hwnd)
{
    // create a file-reading object
    ifstream fin;
    fin.open("schedule.txt"); // open a file
    if (!fin.good())
        return 1; // exit if file not found

    // read each line of the file
    while (!fin.eof())
    {
        // read an entire line into memory
        char buf[MAX_CHARS_PER_LINE];
        fin.getline(buf, MAX_CHARS_PER_LINE);

        // parse the line into double-colon delimited tokens
        int n = 0; // a for-loop index

        // array to store memory addresses of the tokens in buf
        const char* token[MAX_TOKENS_PER_LINE] = {}; // initialize to 0

        // parse the line
        token[0] = strtok(buf, DELIMITER); // first token
        if (token[0]) // zero if line is blank
        {
            for (n = 1; n < MAX_TOKENS_PER_LINE; n++) {
                token[n] = strtok(0, DELIMITER); // subsequent tokens
                if (!token[n]) break; // no more tokens
            }

            // Process tokens
            if (strcmp(token[0], "Video") == 0 && strcmp(token[1], "Custom") == 0) {
                CustomTextVideo* customTextVideo = new CustomTextVideo;
                videoSchedule.emplace_back(customTextVideo);
                int index = videoSchedule.size() - 1;

                for (int i = 0; i < n; i++) {
                    // Create new custom text video object.
                    //cout << i << ":" << token[i] << "\n";

                    if (strcmp(token[i],"Filepath") == 0) {
                        string filepath = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Title") != 0) {
                            filepath = filepath + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Setfilepath(filepath);

                    }
                    if (strcmp(token[i],"Title") == 0) {
                        string title = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line1") != 0) {
                            title = title + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settitle(title);
                    }
                    if (strcmp(token[i],"Line1") == 0 && strcmp(token[i+1],"Line2") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line2") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext1(line);
                    }
                    if (strcmp(token[i],"Line2") == 0 && strcmp(token[i+1],"Line3") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line3") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext2(line);
                    }
                    if (strcmp(token[i],"Line3") == 0 && strcmp(token[i+1],"Line4") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line4") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext3(line);
                    }
                    if (strcmp(token[i],"Line4") == 0 && strcmp(token[i+1],"Line5") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line5") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext4(line);
                    }
                    if (strcmp(token[i],"Line5") == 0 && strcmp(token[i+1],"Line6") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line6") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext5(line);
                    }
                    if (strcmp(token[i],"Line6") == 0 && strcmp(token[i+1],"Line7") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line7") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext6(line);
                    }
                    if (strcmp(token[i],"Line7") == 0 && strcmp(token[i+1],"Line8") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line8") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext7(line);
                    }
                    if (strcmp(token[i],"Line8") == 0 && strcmp(token[i+1],"Line9") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line9") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext8(line);
                    }
                    if (strcmp(token[i],"Line9") == 0 && strcmp(token[i+1],"Line10") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Line10") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext9(line);
                    }
                    if (strcmp(token[i],"Line10") == 0 && strcmp(token[i+1],"AppearLeft") != 0
                        && strcmp(token[i+1],"AppearRight") != 0 && strcmp(token[i+1],"Appear") != 0) {
                        string line = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"AppearLeft") != 0 && strcmp(token[i],"AppearRight") != 0 && strcmp(token[i],"Appear") != 0) {
                            line = line + " " + token[i];
                            i = i + 1;
                        }
                        customTextVideo->Settext10(line);
                    }

                    if (strcmp(token[i],"AppearLeft") == 0) customTextVideo->SetappearDirection(ICD_CUST_RADIO_SLIDE_LEFT);
                    if (strcmp(token[i],"AppearRight") == 0) customTextVideo->SetappearDirection(ICD_CUST_RADIO_SLIDE_RIGHT);
                    if (strcmp(token[i],"Appear") == 0) customTextVideo->SetappearDirection(ICD_CUST_RADIO_APPEAR);
                    if (strcmp(token[i],"AlignLeft") == 0) customTextVideo->SetalignDirection(ICD_CUST_RADIO_ALIGN_LEFT);
                    if (strcmp(token[i],"AlignRight") == 0) customTextVideo->SetalignDirection(ICD_CUST_RADIO_ALIGN_RIGHT);
                    if (strcmp(token[i],"AlignCenter") == 0) customTextVideo->SetalignDirection(ICD_CUST_RADIO_ALIGN_CENTER);
                }
                ((Video*)customTextVideo)->setVideoType(TYPE_CUST_TEXT);

                // Add the new video's title to the list.
                char *newListEntry = (char*)(customTextVideo->Gettitle().c_str());
                SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
            }
            else if (strcmp(token[0], "Video") == 0 && strcmp(token[1], "Temperature") == 0) {
                TemperatureVideo* temperatureVideo = new TemperatureVideo;
                videoSchedule.emplace_back(temperatureVideo);
                int index = videoSchedule.size() - 1;

                for (int i = 2; i < n; i++) {
                    if (strcmp(token[i],"Filepath") == 0) {
                        string filepath = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Temperature") != 0) {
                            filepath = filepath + " " + token[i];
                            i = i + 1;
                        }
                        temperatureVideo->Setfilepath(filepath);

                    }
                    if (strcmp(token[i],"Temperature") == 0) {
                        string temperature = token[i+1];
                        i = i + 2;
                        temperatureVideo->Settemperature(temperature);
                    }
                }
                temperatureVideo->setVideoType(TYPE_TEMP);

                // Add the new video's title to the list.
                string tempTemp = "Temperature: " + temperatureVideo->Gettemperature();
                char *newListEntry = (char*)tempTemp.c_str();
                SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
            }
            else if (strcmp(token[0], "Video") == 0 && strcmp(token[1], "Date") == 0) {
                DateVideo* dateVideo = new DateVideo;
                videoSchedule.emplace_back(dateVideo);
                int index = videoSchedule.size() - 1;

                for (int i = 2; i < n; i++) {
                    if (strcmp(token[i],"Filepath") == 0) {
                        string filepath = token[i+1];
                        i = i + 2;
                        while (strcmp(token[i],"Day") != 0) {
                            filepath = filepath + " " + token[i];
                            i = i + 1;
                        }
                        dateVideo->Setfilepath(filepath);

                    }
                    if (strcmp(token[i],"Day") == 0) {
                        if (strcmp(token[i+1],"True") == 0) dateVideo->Setday(true);
                    }
                    if (strcmp(token[i],"Month") == 0) {
                        if (strcmp(token[i+1],"True") == 0) dateVideo->Setmonth(true);
                    }
                    if (strcmp(token[i],"Year") == 0) {
                        if (strcmp(token[i+1],"True") == 0) dateVideo->Setyear(true);
                    }
                }
                dateVideo->setVideoType(TYPE_DATE);

                // Add the new video's title to the list.
                string tempDate = "Today's Date";
                char *newListEntry = (char*)tempDate.c_str();
                SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
            }
        }
    }
    return 0;
}

BOOL CALLBACK CustomTextDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    switch(Message)
    {
        case WM_INITDIALOG:
        {
            if (editMode == true) {
                SendDlgItemMessage(hwnd, IDC_CUST_FILEPATH, WM_SETTEXT, 0, (LPARAM)(globalVideo.Getfilepath().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TITLE_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettitle().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT1_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext1().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT2_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext2().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT3_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext3().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT4_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext4().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT5_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext5().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT6_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext6().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT7_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext7().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT8_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext8().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT9_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext9().c_str()));
                SendDlgItemMessage(hwnd, IDC_CUST_TEXT10_TXT, WM_SETTEXT, 0, (LPARAM)(globalVideo.Gettext10().c_str()));
                if (globalVideo.GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                }
                else if (globalVideo.GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                }
                else {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                }

                if (globalVideo.GetalignDirection() == ICD_CUST_RADIO_ALIGN_LEFT) {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                }
                else if (globalVideo.GetalignDirection() == ICD_CUST_RADIO_ALIGN_RIGHT) {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                }
                else {
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                }
            }
            return TRUE;
        }

        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_CUST_FILE_BUTTON:
                {
                    char* filePath = openFile(hwnd);
                    SendDlgItemMessage(hwnd, IDC_CUST_FILEPATH, WM_SETTEXT, 0, (LPARAM)filePath);
                    break;
                }
                case ICD_CUST_RADIO_SLIDE_LEFT:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                break;
                case ICD_CUST_RADIO_SLIDE_RIGHT:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                break;
                case ICD_CUST_RADIO_APPEAR:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                break;
                case ICD_CUST_RADIO_ALIGN_LEFT:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                break;
                case ICD_CUST_RADIO_ALIGN_RIGHT:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                break;
                case ICD_CUST_RADIO_ALIGN_CENTER:
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_LEFT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                break;
                case IDC_CUST_OK:
                    {
                    //Assign all the user-entered fields from the dialog into the global video object.
                    char buf[1000];

                    GetDlgItemText(hwnd, IDC_CUST_FILEPATH, buf, 1000);
                    string filepath(buf);
                    if (filepath.length() == 0) {
                        MessageBox(hwnd, "You must specify a video file to play!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }
                    globalVideo.Setfilepath(filepath);
                    GetDlgItemText(hwnd, IDC_CUST_TITLE_TXT, buf, 128);
                    string title(buf);
                    if (title.length() == 0) {
                        MessageBox(hwnd, "You must specify a title for your video!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }
                    globalVideo.Settitle(title);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT1_TXT, buf, 128);
                    string text1(buf);
                    globalVideo.Settext1(text1);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT2_TXT, buf, 128);
                    string text2(buf);
                    globalVideo.Settext2(text2);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT3_TXT, buf, 128);
                    string text3(buf);
                    globalVideo.Settext3(text3);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT4_TXT, buf, 128);
                    string text4(buf);
                    globalVideo.Settext4(text4);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT5_TXT, buf, 128);
                    string text5(buf);
                    globalVideo.Settext5(text5);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT6_TXT, buf, 128);
                    string text6(buf);
                    globalVideo.Settext6(text6);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT7_TXT, buf, 128);
                    string text7(buf);
                    globalVideo.Settext7(text7);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT8_TXT, buf, 128);
                    string text8(buf);
                    globalVideo.Settext8(text8);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT9_TXT, buf, 128);
                    string text9(buf);
                    globalVideo.Settext9(text9);
                    GetDlgItemText(hwnd, IDC_CUST_TEXT10_TXT, buf, 128);
                    string text10(buf);
                    globalVideo.Settext10(text10);

                    if(SendDlgItemMessage(hwnd, ICD_CUST_RADIO_APPEAR, BM_GETCHECK, 0, 0)) {
                        globalVideo.SetappearDirection(ICD_CUST_RADIO_APPEAR);
                    } else if(SendDlgItemMessage(hwnd, ICD_CUST_RADIO_SLIDE_RIGHT, BM_GETCHECK, 0, 0)) {
                        globalVideo.SetappearDirection(ICD_CUST_RADIO_SLIDE_RIGHT);
                    } else {
                        globalVideo.SetappearDirection(ICD_CUST_RADIO_SLIDE_LEFT);
                    }

                    if(SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_CENTER, BM_GETCHECK, 0, 0)) {
                        globalVideo.SetalignDirection(ICD_CUST_RADIO_ALIGN_CENTER);
                    } else if(SendDlgItemMessage(hwnd, ICD_CUST_RADIO_ALIGN_RIGHT, BM_GETCHECK, 0, 0)) {
                        globalVideo.SetalignDirection(ICD_CUST_RADIO_ALIGN_RIGHT);
                    } else {
                        globalVideo.SetalignDirection(ICD_CUST_RADIO_ALIGN_LEFT);
                    }

                    GlobalFree((HANDLE)buf);
                    EndDialog(hwnd, 1);
                    break;
                    }
                case IDC_CUST_CANCEL:
                    EndDialog(hwnd, 0);
                break;
            }
        break;
        default:
            return FALSE;
    }
    return TRUE;
}

BOOL CALLBACK TemperatureDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
    {
        case WM_INITDIALOG:
            if (editMode) {
                SendDlgItemMessage(hwnd, IDC_TEMP_FILEPATH, WM_SETTEXT, 0, (LPARAM)(globalTemperatureVideo.Getfilepath().c_str()));
                SendDlgItemMessage(hwnd, IDC_TEMP_TITLE_TXT, WM_SETTEXT, 0, (LPARAM)(globalTemperatureVideo.Gettemperature().c_str()));
            }
            return TRUE;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_TEMP_FILE_BUTTON:
                {
                    char* filePath = openFile(hwnd);
                    SendDlgItemMessage(hwnd, IDC_TEMP_FILEPATH, WM_SETTEXT, 0, (LPARAM)filePath);
                    break;
                }
                case IDC_TEMP_OK:
                {
                    //Assign all the user-entered fields from the dialog into the global video object.
                    char buf[1000];

                    GetDlgItemText(hwnd, IDC_TEMP_FILEPATH, buf, 1000);
                    string filepath(buf);
                    if (filepath.length() == 0) {
                        MessageBox(hwnd, "You must specify a video file to play!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }
                    globalTemperatureVideo.Setfilepath(filepath);
                    GetDlgItemText(hwnd, IDC_TEMP_TITLE_TXT, buf, 128);
                    string temperature(buf);
                    if (temperature.length() == 0) {
                        MessageBox(hwnd, "You must specify a temperature for your video!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }
                    globalTemperatureVideo.Settemperature(temperature);

                    GlobalFree((HANDLE)buf);
                    EndDialog(hwnd, 1);
                    break;
                }
                case IDC_TEMP_CANCEL:
                    EndDialog(hwnd, 0);
                    break;
            }
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

BOOL CALLBACK DateDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
    {
        case WM_INITDIALOG:
            if (editMode == true) {
                SendDlgItemMessage(hwnd, IDC_DATE_FILEPATH, WM_SETTEXT, 0, (LPARAM)(globalDateVideo.Getfilepath().c_str()));
                if(globalDateVideo.Getday()) SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                if(globalDateVideo.Getmonth()) SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                if(globalDateVideo.Getyear()) SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
            }
            return TRUE;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_DATE_FILE_BUTTON:
                {
                    char* filePath = openFile(hwnd);
                    SendDlgItemMessage(hwnd, IDC_DATE_FILEPATH, WM_SETTEXT, 0, (LPARAM)filePath);
                    break;
                }
                case IDC_DATE_DAY_CHECK:
                {
                    if(SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_GETCHECK, 0, 0)) {
                        SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    } else {
                        SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    }
                    break;
                }
                case IDC_DATE_MONTH_CHECK:
                {
                    if(SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_GETCHECK, 0, 0)) {
                        SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    } else {
                        SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    }
                    break;
                }
                case IDC_DATE_YEAR_CHECK:
                {
                    if(SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_GETCHECK, 0, 0)) {
                        SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                    } else {
                        SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                    }
                    break;
                }
                case IDC_DATE_OK:
                {
                    //Assign all the user-entered fields from the dialog into the global video object.
                    char buf[1000];

                    GetDlgItemText(hwnd, IDC_DATE_FILEPATH, buf, 1000);
                    string filepath(buf);
                    if (filepath.length() == 0) {
                        MessageBox(hwnd, "You must specify a video file to play!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }
                    globalDateVideo.Setfilepath(filepath);

                    if(SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_GETCHECK, 0, 0)) {
                        globalDateVideo.Setday(true);
                    } else globalDateVideo.Setday(false);

                    if(SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_GETCHECK, 0, 0)) {
                        globalDateVideo.Setmonth(true);
                    } else globalDateVideo.Setmonth(false);

                    if(SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_GETCHECK, 0, 0)) {
                        globalDateVideo.Setyear(true);
                    } else globalDateVideo.Setyear(false);

                    if (!(SendDlgItemMessage(hwnd, IDC_DATE_DAY_CHECK, BM_GETCHECK, 0, 0)) &&
                        !(SendDlgItemMessage(hwnd, IDC_DATE_MONTH_CHECK, BM_GETCHECK, 0, 0)) &&
                        !(SendDlgItemMessage(hwnd, IDC_DATE_YEAR_CHECK, BM_GETCHECK, 0, 0))) {
                        MessageBox(hwnd, "You must show some part of today's date!", "Error!", MB_ICONEXCLAMATION | MB_OK);
                        return FALSE;
                    }

                    GlobalFree((HANDLE)buf);
                    EndDialog(hwnd, 1);
                    break;
                }
                case IDC_DATE_CANCEL:
                    EndDialog(hwnd, 0);
                    break;
            }
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    switch(Message)
    {
    case WM_INITDIALOG:
    {
        loadConfigFile(hwnd);
        loadVideoScheduleToFile(hwnd);
        if(randomOrder) {
            SendDlgItemMessage(hwnd, IDC_RANDOM, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
        }
        break;
    }
    case WM_CLOSE:
        saveVideoScheduleToFile();
        DestroyWindow(hwnd);
    break;
    case WM_DESTROY:
        DestroyWindow(g_mainDialog);
        PostQuitMessage(0);
    break;
    case WM_COMMAND:
        switch(LOWORD(wParam))
        {
            case IDC_QUIT:
                saveVideoScheduleToFile();
                DestroyWindow(hwnd);
            break;
            case IDC_CUSTOM_TEXT:
            {
                if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_CUSTOM_TEXT_DLG), hwnd, CustomTextDlgProc) == 1){
                    HWND hList = GetDlgItem(hwnd, IDC_LIST);
                    int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                    if (number == 100) {
                        MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                            MB_ICONEXCLAMATION | MB_OK);
                        return 0;
                    }

                    // Copy global video into new CustomTextVideo object.
                    CustomTextVideo* customTextVideo = new CustomTextVideo;
                    videoSchedule.emplace_back(customTextVideo);
                    int index = videoSchedule.size() - 1;

                    customTextVideo->Setfilepath(globalVideo.Getfilepath());
                    customTextVideo->Settitle(globalVideo.Gettitle());
                    customTextVideo->Settext1(globalVideo.Gettext1());
                    customTextVideo->Settext2(globalVideo.Gettext2());
                    customTextVideo->Settext3(globalVideo.Gettext3());
                    customTextVideo->Settext4(globalVideo.Gettext4());
                    customTextVideo->Settext5(globalVideo.Gettext5());
                    customTextVideo->Settext6(globalVideo.Gettext6());
                    customTextVideo->Settext7(globalVideo.Gettext7());
                    customTextVideo->Settext8(globalVideo.Gettext8());
                    customTextVideo->Settext9(globalVideo.Gettext9());
                    customTextVideo->Settext10(globalVideo.Gettext10());
                    customTextVideo->SetalignDirection(globalVideo.GetalignDirection());
                    customTextVideo->SetappearDirection(globalVideo.GetappearDirection());
                    ((Video*)customTextVideo)->setVideoType(TYPE_CUST_TEXT);

                    // Add the new video's title to the list.
                    char *newListEntry = (char*)globalVideo.Gettitle().c_str();
                    SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
                }
            break;
            }
            case IDC_TEMPERATURE:
            {
                if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_TEMPERATURE_DLG), hwnd, TemperatureDlgProc) == 1){
                    HWND hList = GetDlgItem(hwnd, IDC_LIST);
                    int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                    if (number == 100) {
                        MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                            MB_ICONEXCLAMATION | MB_OK);
                        return 0;
                    }

                    // Copy global video into new TemperatureVideo object.
                    TemperatureVideo* temperatureVideo = new TemperatureVideo;
                    videoSchedule.emplace_back(temperatureVideo);
                    int index = videoSchedule.size() - 1;

                    temperatureVideo->Setfilepath(globalTemperatureVideo.Getfilepath());
                    temperatureVideo->Settemperature(globalTemperatureVideo.Gettemperature());
                    temperatureVideo->setVideoType(TYPE_TEMP);

                    // Add the new video's title to the list.
                    string tempTemp = "Temperature: " + globalTemperatureVideo.Gettemperature();
                    char *newListEntry = (char*)tempTemp.c_str();
                    SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
                }
                break;
            }
            case IDC_DATE:
            {
                if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DATE_DLG), hwnd, DateDlgProc) == 1){
                    HWND hList = GetDlgItem(hwnd, IDC_LIST);
                    int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                    if (number == 100) {
                        MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                            MB_ICONEXCLAMATION | MB_OK);
                        return 0;
                    }

                    // Copy global video into new DateVideo object.
                    DateVideo* dateVideo = new DateVideo;
                    videoSchedule.emplace_back(dateVideo);
                    int index = videoSchedule.size() - 1;

                    dateVideo->Setfilepath(globalDateVideo.Getfilepath());
                    dateVideo->Setday(globalDateVideo.Getday());
                    dateVideo->Setmonth(globalDateVideo.Getmonth());
                    dateVideo->Setyear(globalDateVideo.Getyear());
                    dateVideo->setVideoType(TYPE_DATE);

                    // Add the new video's title to the list.
                    string tempDate = "Today's Date";
                    char *newListEntry = (char*)tempDate.c_str();
                    SendDlgItemMessage(hwnd, IDC_LIST, LB_ADDSTRING, 0, (LPARAM)newListEntry);
                }
                break;
            }
            case IDC_EDIT:
            {
                HWND hList = GetDlgItem(hwnd, IDC_LIST);
                int numItems = SendMessage(hList, LB_GETCOUNT, 0, 0);
                int index = -1;
                int newIndex = -1;

                // Go through the items and find the first selected one
                for (int i = 0; i < numItems; i++)
                {
                    // Check if this item is selected or not..
                    if (SendMessage(hList, LB_GETSEL, i, 0) > 0)
                    {
                        // We only want the first selected so break.
                        index = i;
                        break;
                    }
                }
                Video* editVideo = (videoSchedule.at(index)).get();
                if (editVideo->getVideoType() == TYPE_CUST_TEXT) {
                    CustomTextVideo* customTextVideo = (CustomTextVideo*)editVideo;
                    globalVideo.Setfilepath(customTextVideo->Getfilepath());
                    globalVideo.Settitle(customTextVideo->Gettitle());
                    globalVideo.Settext1(customTextVideo->Gettext1());
                    globalVideo.Settext2(customTextVideo->Gettext2());
                    globalVideo.Settext3(customTextVideo->Gettext3());
                    globalVideo.Settext4(customTextVideo->Gettext4());
                    globalVideo.Settext5(customTextVideo->Gettext5());
                    globalVideo.Settext6(customTextVideo->Gettext6());
                    globalVideo.Settext7(customTextVideo->Gettext7());
                    globalVideo.Settext8(customTextVideo->Gettext8());
                    globalVideo.Settext9(customTextVideo->Gettext9());
                    globalVideo.Settext10(customTextVideo->Gettext10());
                    globalVideo.SetalignDirection(customTextVideo->GetalignDirection());
                    globalVideo.SetappearDirection(customTextVideo->GetappearDirection());

                    editMode = true;

                    if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_CUSTOM_TEXT_DLG), hwnd, CustomTextDlgProc) == 1){
                        HWND hList = GetDlgItem(hwnd, IDC_LIST);
                        int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                        if (number == 100) {
                            MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                                MB_ICONEXCLAMATION | MB_OK);
                            return 0;
                        }

                        if (index != LB_ERR) {
                            unique_ptr<Video> updatedVideo = move(videoSchedule.at(index));

                            updatedVideo.get()->Setfilepath(globalVideo.Getfilepath());
                            ((CustomTextVideo*)updatedVideo.get())->Settitle(globalVideo.Gettitle());
                            ((CustomTextVideo*)updatedVideo.get())->Settext1(globalVideo.Gettext1());
                            ((CustomTextVideo*)updatedVideo.get())->Settext2(globalVideo.Gettext2());
                            ((CustomTextVideo*)updatedVideo.get())->Settext3(globalVideo.Gettext3());
                            ((CustomTextVideo*)updatedVideo.get())->Settext4(globalVideo.Gettext4());
                            ((CustomTextVideo*)updatedVideo.get())->Settext5(globalVideo.Gettext5());
                            ((CustomTextVideo*)updatedVideo.get())->Settext6(globalVideo.Gettext6());
                            ((CustomTextVideo*)updatedVideo.get())->Settext7(globalVideo.Gettext7());
                            ((CustomTextVideo*)updatedVideo.get())->Settext8(globalVideo.Gettext8());
                            ((CustomTextVideo*)updatedVideo.get())->Settext9(globalVideo.Gettext9());
                            ((CustomTextVideo*)updatedVideo.get())->Settext10(globalVideo.Gettext10());
                            ((CustomTextVideo*)updatedVideo.get())->SetalignDirection(globalVideo.GetalignDirection());
                            ((CustomTextVideo*)updatedVideo.get())->SetappearDirection(globalVideo.GetappearDirection());
                            (updatedVideo.get())->setVideoType(TYPE_CUST_TEXT);

                            videoSchedule.at(index)=move(updatedVideo);

                            // Delete the old video, then add the new video's title to the list in the same index.
                            SendMessage(hList, LB_DELETESTRING, index, 0);

                            char *newListEntry = (char*)globalVideo.Gettitle().c_str();
                            SendDlgItemMessage(hwnd, IDC_LIST, LB_INSERTSTRING, index, (LPARAM)newListEntry);
                        }
                    }
                    editMode = false;
                } else if (editVideo->getVideoType() == TYPE_TEMP) {
                    TemperatureVideo* tempVideo = (TemperatureVideo*)editVideo;
                    globalTemperatureVideo.Setfilepath(tempVideo->Getfilepath());
                    globalTemperatureVideo.Settemperature(tempVideo->Gettemperature());

                    editMode = true;

                    if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_TEMPERATURE_DLG), hwnd, TemperatureDlgProc) == 1){
                        HWND hList = GetDlgItem(hwnd, IDC_LIST);
                        int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                        if (number == 100) {
                            MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                                MB_ICONEXCLAMATION | MB_OK);
                            return 0;
                        }

                        if (index != LB_ERR) {
                            unique_ptr<Video> updatedVideo = move(videoSchedule.at(index));

                            updatedVideo.get()->Setfilepath(globalTemperatureVideo.Getfilepath());
                            ((TemperatureVideo*)updatedVideo.get())->Settemperature(globalTemperatureVideo.Gettemperature());
                            ((TemperatureVideo*)updatedVideo.get())->setVideoType(TYPE_TEMP);

                            videoSchedule.at(index)=move(updatedVideo);

                            // Delete the old video, then add the new video's title to the list in the same index.
                            SendMessage(hList, LB_DELETESTRING, index, 0);

                            // Add the new video's title to the list.
                            string tempTemp = "Temperature: " + ((TemperatureVideo*)updatedVideo.get())->Gettemperature();
                            char *newListEntry = (char*)tempTemp.c_str();
                            SendDlgItemMessage(hwnd, IDC_LIST, LB_INSERTSTRING, index, (LPARAM)newListEntry);
                        }
                    }
                    editMode = false;
                } else if (editVideo->getVideoType() == TYPE_DATE) {
                    DateVideo* dateVideo = (DateVideo*)editVideo;
                    globalDateVideo.Setfilepath(dateVideo->Getfilepath());
                    globalDateVideo.Setday(dateVideo->Getday());
                    globalDateVideo.Setmonth(dateVideo->Getmonth());
                    globalDateVideo.Setyear(dateVideo->Getyear());

                    editMode = true;

                    if (DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DATE_DLG), hwnd, DateDlgProc) == 1){
                        HWND hList = GetDlgItem(hwnd, IDC_LIST);
                        int number = SendMessage(hList, LB_GETCOUNT, 0, 0);

                        if (number == 100) {
                            MessageBox(NULL, "You can't have more than 100 videos!", "Error!",
                                MB_ICONEXCLAMATION | MB_OK);
                            return 0;
                        }

                        if (index != LB_ERR) {
                            unique_ptr<Video> updatedVideo = move(videoSchedule.at(index));

                            updatedVideo.get()->Setfilepath(globalDateVideo.Getfilepath());
                            ((DateVideo*)updatedVideo.get())->Setday(globalDateVideo.Getday());
                            ((DateVideo*)updatedVideo.get())->Setmonth(globalDateVideo.Getmonth());
                            ((DateVideo*)updatedVideo.get())->Setyear(globalDateVideo.Getyear());
                            ((DateVideo*)updatedVideo.get())->setVideoType(TYPE_DATE);

                            videoSchedule.at(index)=move(updatedVideo);

                            // Delete the old video, then add the new video's title to the list in the same index.
                            SendMessage(hList, LB_DELETESTRING, index, 0);

                            // Add the new video's title to the list.
                            string tempDate = "Today's Date";
                            char *newListEntry = (char*)tempDate.c_str();
                            SendDlgItemMessage(hwnd, IDC_LIST, LB_INSERTSTRING, index, (LPARAM)newListEntry);
                        }
                    }
                    editMode = false;
                }
                break;
            }
            case IDC_UP:
            {
                HWND hList = GetDlgItem(hwnd, IDC_LIST);
                int numItems = SendMessage(hList, LB_GETCOUNT, 0, 0);
                int index = -1;
                int newIndex = -1;
                char returnedText[1000];

                // Go through the items and find the first selected one
                for (int i = 0; i < numItems; i++)
                {
                    // Check if this item is selected or not..
                    if (SendMessage(hList, LB_GETSEL, i, 0) > 0)
                    {
                        // We only want the first selected so break.
                        index = i;
                        break;
                    }
                }

                if (index > 0) {
                    // get the text of the selected item
                    SendMessage(hList, LB_GETTEXT, (WPARAM)index , (LPARAM)returnedText);
                    string result(returnedText);

                    //Delete the string from the list control.
                    SendMessage(hList, LB_DELETESTRING, (WPARAM)index, 0);

                    //Re-insert the string at the appropriate spot in the list.
                    newIndex = index - 1;
                    SendMessage(hList, LB_INSERTSTRING, (WPARAM)(newIndex), (LPARAM)((char*)result.c_str()));

                    //Re-order the video schedule list.
                    unique_ptr<Video> temp[2];
                    temp[0]=move(videoSchedule.at(index));
                    temp[1]=move(videoSchedule.at(newIndex));

                    videoSchedule.at(index)=move(temp[1]);
                    videoSchedule.at(newIndex)=move(temp[0]);
                }
                break;
            }
            case IDC_DOWN:
            {
                HWND hList = GetDlgItem(hwnd, IDC_LIST);
                int numItems = SendMessage(hList, LB_GETCOUNT, 0, 0);
                int index = -1;
                int numItemsOffset = -1;
                int newIndex = -1;
                char returnedText[1000];

                // Go through the items and find the first selected one
                for (int i = 0; i < numItems; i++)
                {
                    // Check if this item is selected or not..
                    if (SendMessage(hList, LB_GETSEL, i, 0) > 0)
                    {
                        // We only want the first selected so break.
                        index = i;
                        numItemsOffset = numItems - 1;
                        break;
                    }
                }

                if (index < numItemsOffset && numItems != 1) {
                    // get the text of the selected item
                    SendMessage(hList, LB_GETTEXT, (WPARAM)index , (LPARAM)returnedText);
                    string result(returnedText);

                    //Delete the string from the list control.
                    SendMessage(hList, LB_DELETESTRING, (WPARAM)index, 0);

                    //Re-insert the string at the appropriate spot in the list.
                    newIndex = index + 1;
                    SendMessage(hList, LB_INSERTSTRING, (WPARAM)(newIndex), (LPARAM)((char*)result.c_str()));

                    //Re-order the video schedule list.
                    unique_ptr<Video> temp[2];
                    temp[0]=move(videoSchedule.at(index));
                    temp[1]=move(videoSchedule.at(newIndex));

                    videoSchedule.at(index)=move(temp[1]);
                    videoSchedule.at(newIndex)=move(temp[0]);
                }
                break;
            }
            case IDC_LOOP_CHECK:
            {
                if(SendDlgItemMessage(hwnd, IDC_LOOP_CHECK, BM_GETCHECK, 0, 0)) {
                    SendDlgItemMessage(hwnd, IDC_LOOP_CHECK, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                } else {
                    SendDlgItemMessage(hwnd, IDC_LOOP_CHECK, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                }
                break;
            }
            case IDC_RANDOM:
            {
                if(SendDlgItemMessage(hwnd, IDC_RANDOM, BM_GETCHECK, 0, 0)) {
                    SendDlgItemMessage(hwnd, IDC_RANDOM, BM_SETCHECK, (WPARAM)BST_UNCHECKED, 0);
                } else {
                    SendDlgItemMessage(hwnd, IDC_RANDOM, BM_SETCHECK, (WPARAM)BST_CHECKED, 0);
                }
                break;
            }
            case IDC_DELETE:
            {
                HWND hList = GetDlgItem(hwnd, IDC_LIST);
                int index = SendMessage(hList, LB_GETCURSEL, 0, 0);
                if (index != LB_ERR) {
                    //Delete the string from the list control.
                    SendMessage(hList, LB_DELETESTRING, index, 0);
                    //Delete the video object from the video schedule.
                    unique_ptr<Video> removedVideo = move(videoSchedule.at(index));
                    removedVideo.release();
                    videoSchedule.erase(videoSchedule.begin()+index);
                }
            break;
            }
            case IDC_PLAY:
            {
                bool randomOrder = SendDlgItemMessage(hwnd, IDC_RANDOM, BM_GETCHECK, 0, 0);
                showVisual(&videoSchedule, randomOrder);
                break;
            }

            break;
        }
    break;
    default:
        return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hwnd;
    MSG Msg;

    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = NULL;
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = g_szClassName;
    wc.hIconSm       = NULL;

    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, "Window Registration Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    hwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        g_szClassName,
        "EasySandwich v1.0.0",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 0, 0,
        NULL, NULL, hInstance, NULL);

    if(hwnd == NULL)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }
    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    g_mainDialog = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_MAIN_DIALOG), hwnd, MainDlgProc);

    if(g_mainDialog == NULL)
    {
        MessageBox(NULL, "Main Dialog Creation Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    } else {
        ShowWindow(g_mainDialog, SW_SHOW);
    }

    while(GetMessage(&Msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }
    return Msg.wParam;
}
