#ifndef IDC_STATIC
#define IDC_STATIC 112
#endif

#define DLG_MAIN        100
#define IDD_ABOUT       101
#define IDD_MAIN_DIALOG 102

#define IDC_PRESS       103
#define IDC_OTHER       104

#define IDC_CUSTOM_TEXT     105
#define IDC_TEMPERATURE     106
#define IDC_DATE            107
#define IDC_LIST            108
#define IDC_LOOP_CHECK      109
#define IDC_UP              110
#define IDC_DOWN            111
#define IDC_EDIT            112
#define IDC_DELETE          113
#define IDC_PLAY            114
#define IDC_QUIT            115
#define IDC_RANDOM          116

#define IDD_CUSTOM_TEXT_DLG      117
#define IDC_CUST_TITLE_TXT_L     118
#define IDC_CUST_TITLE_TXT       119
#define IDC_CUST_TEXT1_TXT_L     120
#define IDC_CUST_TEXT1_TXT       121
#define IDC_CUST_TEXT2_TXT_L     122
#define IDC_CUST_TEXT2_TXT       123
#define IDC_CUST_TEXT3_TXT_L     124
#define IDC_CUST_TEXT3_TXT       125
#define IDC_CUST_TEXT4_TXT_L     126
#define IDC_CUST_TEXT4_TXT       127
#define IDC_CUST_TEXT5_TXT_L     128
#define IDC_CUST_TEXT5_TXT       129
#define IDC_CUST_TEXT6_TXT_L     130
#define IDC_CUST_TEXT6_TXT       131
#define IDC_CUST_TEXT7_TXT_L     132
#define IDC_CUST_TEXT7_TXT       133
#define IDC_CUST_TEXT8_TXT_L     134
#define IDC_CUST_TEXT8_TXT       135
#define IDC_CUST_TEXT9_TXT_L     136
#define IDC_CUST_TEXT9_TXT       137
#define IDC_CUST_TEXT10_TXT_L    138
#define IDC_CUST_TEXT10_TXT      139
#define IDC_CUST_FILEPATH        140
#define IDC_CUST_FILE_BUTTON     141
#define IDC_CUST_OK              142
#define IDC_CUST_CANCEL          143
#define ICD_CUST_RADIO_SLIDE_LEFT   144
#define ICD_CUST_RADIO_SLIDE_RIGHT  145
#define ICD_CUST_RADIO_APPEAR       146
#define ICD_CUST_RADIO_ALIGN_LEFT   147
#define ICD_CUST_RADIO_ALIGN_RIGHT  148
#define ICD_CUST_RADIO_ALIGN_CENTER 149

#define IDD_TEMPERATURE_DLG     150
#define IDC_TEMP_FILEPATH       151
#define IDC_TEMP_FILE_BUTTON    152
#define IDC_TEMPERATURE_TXT_L   153
#define IDC_TEMP_TITLE_TXT      154
#define IDC_TEMP_OK             155
#define IDC_TEMP_CANCEL         156

#define IDD_DATE_DLG            157
#define IDC_DATE_FILEPATH       158
#define IDC_DATE_FILE_BUTTON    159
#define IDC_DATE_DAY_CHECK      160
#define IDC_DATE_MONTH_CHECK    161
#define IDC_DATE_YEAR_CHECK     162
#define IDC_DATE_OK             163
#define IDC_DATE_CANCEL         164

#define TYPE_CUST_TEXT          8000
#define TYPE_TEMP               8001
#define TYPE_DATE               8002

#define ID_FILE_EXIT 9001
