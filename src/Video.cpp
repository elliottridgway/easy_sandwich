#include "CustomTextVideo.h"

/**
 *  Video constructors, destructors, and methods.
 */

// Default constructor for a Video.
Video::Video() {}

// Default destructor for a Video.
Video::~Video() {}

// Sets the type for the Video.
void Video::setVideoType(int val) {
    type = val;
}

// Gets the type for the Video.
int Video::getVideoType() {
    return type;
}

// Gets the filepath of the Video.
std::string Video::Getfilepath() {
    return filepath;
}

// Sets the filepath of the Video.
void Video::Setfilepath(std::string val) {
    filepath = val;
}

/**
 *  TemperatureVideo constructors, destructors, and methods.
 */

// Default constructor for a TemperatureVideo.
TemperatureVideo::TemperatureVideo() {}

// Default destructor for a TemperatureVideo.
TemperatureVideo::~TemperatureVideo() {}

// Gets the temperature.
std::string TemperatureVideo::Gettemperature() {
    return temperature;
}

// Sets the temperature.
void TemperatureVideo::Settemperature(std::string val) {
    temperature = val;
}

// Outputs information about the TemperatureVideo.  Should be used for debugging purposes only.
std::string TemperatureVideo::print() {
    std::string videoScheduleString = "Video Temperature";
    videoScheduleString = videoScheduleString + " Filepath " + Getfilepath();
    videoScheduleString = videoScheduleString + " Temperature " + temperature;
    videoScheduleString = videoScheduleString + "\n";
    return videoScheduleString;
}

/**
 *  DateVideo constructors, destructors, and methods.
 */

// Default constructor for a DateVideo.
DateVideo::DateVideo() {}

// Default destructor for a DateVideo.
DateVideo::~DateVideo() {}

// Gets the boolean that dictates whether the day of the week should be shown.
bool DateVideo::Getday() {
    return day;
}

// Sets the boolean that dictates whether the day of the week should be shown.
void DateVideo::Setday(bool val) {
    day = val;
}

// Gets the boolean that dictates whether the month should be shown.
bool DateVideo::Getmonth() {
    return month;
}

// Sets the boolean that dictates whether the month should be shown.
void DateVideo::Setmonth(bool val) {
    month = val;
}

// Gets the boolean that dictates whether the year should be shown.
bool DateVideo::Getyear() {
    return year;
}

// Sets the boolean that dictates whether the year should be shown.
void DateVideo::Setyear(bool val) {
    year = val;
}

// Outputs information about the DateVideo.  Should be used for debugging purposes only.
std::string DateVideo::print() {
    std::string videoScheduleString = "Video Date";
    videoScheduleString = videoScheduleString + " Filepath " + Getfilepath();
    if (day == true) videoScheduleString = videoScheduleString + " Day True";
    else videoScheduleString = videoScheduleString + " Day False";
    if (month == true) videoScheduleString = videoScheduleString + " Month True";
    else videoScheduleString = videoScheduleString + " Month False";
    if (year == true) videoScheduleString = videoScheduleString + " Year True";
    else videoScheduleString = videoScheduleString + " Year False";
    videoScheduleString = videoScheduleString + "\n";
    return videoScheduleString;
}

/**
 *  CustomTextVideo constructors, destructors, and methods.
 */

// Constructor for a CustomTextVideo.
CustomTextVideo::CustomTextVideo()
{
    filepath = "";
    title = "";
    text1 = "";
    text2 = "";
    text3 = "";
    text4 = "";
    text5 = "";
    text6 = "";
    text7 = "";
    text8 = "";
    text9 = "";
    text10 = "";
    appearDirection = 0;
    alignDirection = 0;
}

// Default destructor for a CustomTextVideo.
CustomTextVideo::~CustomTextVideo() {}

// Gets the CustomTextVideo title.
std::string CustomTextVideo::Gettitle() {
    return title;
}

// Sets the CustomTextVideo title.
void CustomTextVideo::Settitle(std::string val) {
    title = val;
}

//  Getters and setters for the CustomTextVideo's
//  ten text strings.

std::string CustomTextVideo::Gettext1() {
    return text1;
}
void CustomTextVideo::Settext1(std::string val) {
    text1 = val;
}
std::string CustomTextVideo::Gettext2() {
    return text2;
}
void CustomTextVideo::Settext2(std::string val) {
    text2 = val;
}
std::string CustomTextVideo::Gettext3() {
    return text3;
}
void CustomTextVideo::Settext3(std::string val) {
    text3 = val;
}
std::string CustomTextVideo::Gettext4() {
    return text4;
}
void CustomTextVideo::Settext4(std::string val) {
    text4 = val;
}
std::string CustomTextVideo::Gettext5() {
    return text5;
}
void CustomTextVideo::Settext5(std::string val) {
    text5 = val;
}
std::string CustomTextVideo::Gettext6() {
    return text6;
}
void CustomTextVideo::Settext6(std::string val) {
    text6 = val;
}
std::string CustomTextVideo::Gettext7() {
    return text7;
}
void CustomTextVideo::Settext7(std::string val) {
    text7 = val;
}
std::string CustomTextVideo::Gettext8() {
    return text8;
}
void CustomTextVideo::Settext8(std::string val) {
    text8 = val;
}
std::string CustomTextVideo::Gettext9() {
    return text9;
}
void CustomTextVideo::Settext9(std::string val) {
    text9 = val;
}
std::string CustomTextVideo::Gettext10() {
    return text10;
}
void CustomTextVideo::Settext10(std::string val) {
    text10 = val;
}

// Gets the CustomTextVideo's appear direction.  This dictates which side of
// the screen the text slides in from.
int CustomTextVideo::GetappearDirection() {
    return appearDirection;
}

// Sets the CustomTextVideo's appear direction.  This dictates which side of
// the screen the text slides in from.
void CustomTextVideo::SetappearDirection(int val) {
    appearDirection = val;
}

// Gets the CustomTextVideo's align direction.  This dictates which side of the
// screen the text aligns with.
int CustomTextVideo::GetalignDirection() {
    return alignDirection;
}

// Sets the CustomTextVideo's align direction.  This dictates which side of the
// screen the text aligns with.
void CustomTextVideo::SetalignDirection(int val) {
    alignDirection = val;
}

// Outputs information about the CustomTextVideo.  Should be used for debugging purposes only.
std::string CustomTextVideo::print()
{
    std::string videoScheduleString = "Video Custom";
    videoScheduleString = videoScheduleString + " Filepath " + Getfilepath();
    videoScheduleString = videoScheduleString + " Title " + title;
    videoScheduleString = videoScheduleString + " Line1 ";
    if(text1.size() > 0) videoScheduleString = videoScheduleString + text1;
    videoScheduleString = videoScheduleString + " Line2 ";
    if(text2.size() > 0) videoScheduleString = videoScheduleString + text2;
    videoScheduleString = videoScheduleString + " Line3 ";
    if(text3.size() > 0) videoScheduleString = videoScheduleString + text3;
    videoScheduleString = videoScheduleString + " Line4 ";
    if(text4.size() > 0) videoScheduleString = videoScheduleString + text4;
    videoScheduleString = videoScheduleString + " Line5 ";
    if(text5.size() > 0) videoScheduleString = videoScheduleString + text5;
    videoScheduleString = videoScheduleString + " Line6 ";
    if(text6.size() > 0) videoScheduleString = videoScheduleString + text6;
    videoScheduleString = videoScheduleString + " Line7 ";
    if(text7.size() > 0) videoScheduleString = videoScheduleString + text7;
    videoScheduleString = videoScheduleString + " Line8 ";
    if(text8.size() > 0) videoScheduleString = videoScheduleString + text8;
    videoScheduleString = videoScheduleString + " Line9 ";
    if(text9.size() > 0) videoScheduleString = videoScheduleString + text9;
    videoScheduleString = videoScheduleString + " Line10 ";
    if(text10.size() > 0) videoScheduleString = videoScheduleString + text10;

    if(appearDirection == ICD_CUST_RADIO_SLIDE_LEFT) videoScheduleString = videoScheduleString + " AppearLeft";
    else if(appearDirection == ICD_CUST_RADIO_SLIDE_RIGHT) videoScheduleString = videoScheduleString + " AppearRight";
    else videoScheduleString = videoScheduleString + " Appear";

    if(alignDirection == ICD_CUST_RADIO_ALIGN_LEFT) videoScheduleString = videoScheduleString + " AlignLeft";
    else if(alignDirection == ICD_CUST_RADIO_ALIGN_RIGHT) videoScheduleString = videoScheduleString + " AlignRight";
    else videoScheduleString = videoScheduleString + " AlignCenter";

    videoScheduleString = videoScheduleString + "\n";

    return videoScheduleString;
}

