#ifndef VISUAL_LOGIC_H_INCLUDED
#define VISUAL_LOGIC_H_INCLUDED

//Using SDL, SDL_image, standard IO, and strings
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <ctime>

#ifndef SHARED_RESOURCES_INCLUDED
#define SHARED_RESOURCES_INCLUDED

#include <vector>
#include "CustomTextVideo.h"

#endif

//Screen dimension constants
int SCREEN_WIDTH = 0;
int SCREEN_HEIGHT = 0;
std::string titleFontPath;
std::string mainFontPath;

//Timer
int timer = 0;

//Date calculation variables
int tomorrow_day = 0;
std::string dayNumber;
std::string dayOfWeek;
std::string month;
int year;
std::string yearString;
std::string finalDateString;

//Texture wrapper class
class LTexture
{
	public:
		//Initializes variables
		LTexture();

		//Deallocates memory
		~LTexture();

		//Loads image at specified path
		bool loadFromFile( std::string path );

        //Creates image from font string
		bool loadFromRenderedText( TTF_Font *font, std::string textureText, SDL_Color textColor);

		//Deallocates texture
		void free();

		//Set color modulation
		void setColor( Uint8 red, Uint8 green, Uint8 blue );

		//Set blending
		void setBlendMode( SDL_BlendMode blending );

		//Set alpha modulation
		void setAlpha( Uint8 alpha );

		//Renders texture at given point
		void render( int x, int y, SDL_Rect* clip = NULL );

		// Get the texture
		SDL_Texture* getTexture();

		//Gets image dimensions
		int getWidth();
		int getHeight();

	private:
		//The actual hardware texture
		SDL_Texture* mTexture;

		//Image dimensions
		int mWidth;
		int mHeight;
};

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Globally used fonts
TTF_Font *titleFont = NULL;
TTF_Font *gFont = NULL;

//Rendered textures
LTexture gTitleTexture;
LTexture gTextTexture1;
LTexture gTextTexture2;
LTexture gTextTexture3;
LTexture gTextTexture4;
LTexture gTextTexture5;
LTexture gTextTexture6;
LTexture gTextTexture7;
LTexture gTextTexture8;
LTexture gTextTexture9;
LTexture gTextTexture10;

LTexture gTitleOutlineTexture;
LTexture gTextOutlineTexture1;
LTexture gTextOutlineTexture2;
LTexture gTextOutlineTexture3;
LTexture gTextOutlineTexture4;
LTexture gTextOutlineTexture5;
LTexture gTextOutlineTexture6;
LTexture gTextOutlineTexture7;
LTexture gTextOutlineTexture8;
LTexture gTextOutlineTexture9;
LTexture gTextOutlineTexture10;

LTexture gTextTemperature1;
LTexture gTextTemperatureOutline1;
LTexture gTextTemperature2;
LTexture gTextTemperatureOutline2;
LTexture gTextTemperature3;
LTexture gTextTemperatureOutline3;

LTexture gTextDate1;
LTexture gTextDateOutline1;
LTexture gTextDate2;
LTexture gTextDateOutline2;
LTexture gTextDate3;
LTexture gTextDateOutline3;

//Scene textures (there is a limit of five textures per scene)
LTexture texture1;
LTexture texture2;
LTexture texture3;
LTexture texture4;
LTexture texture5;

// Event moments
int TITLE_FADE_IN_START = 0;
int TITLE_FADE_IN_END = 0;
int TEXT1_MOVEMENT_START = 0;
int TEXT1_MOVEMENT_END = 0;
int TEXT2_MOVEMENT_START = 0;
int TEXT2_MOVEMENT_END = 0;
int TEXT3_MOVEMENT_START = 0;
int TEXT3_MOVEMENT_END = 0;
int TEXT4_MOVEMENT_START = 0;
int TEXT4_MOVEMENT_END = 0;
int TEXT5_MOVEMENT_START = 0;
int TEXT5_MOVEMENT_END = 0;
int TEXT6_MOVEMENT_START = 0;
int TEXT6_MOVEMENT_END = 0;
int TEXT7_MOVEMENT_START = 0;
int TEXT7_MOVEMENT_END = 0;
int TEXT8_MOVEMENT_START = 0;
int TEXT8_MOVEMENT_END = 0;
int TEXT9_MOVEMENT_START = 0;
int TEXT9_MOVEMENT_END = 0;
int TEXT10_MOVEMENT_START = 0;
int TEXT10_MOVEMENT_END = 0;

// Vertical text positions
int TITLE_X_POSITION = 0;
int TEXT1_X_POSITION = 0;
int TEXT2_X_POSITION = 0;
int TEXT3_X_POSITION = 0;
int TEXT4_X_POSITION = 0;
int TEXT5_X_POSITION = 0;
int TEXT6_X_POSITION = 0;
int TEXT7_X_POSITION = 0;
int TEXT8_X_POSITION = 0;
int TEXT9_X_POSITION = 0;
int TEXT10_X_POSITION = 0;

int TITLE_CURRENT_POSITION = 0;
int TEXT1_CURRENT_POSITION = 0;
int TEXT2_CURRENT_POSITION = 0;
int TEXT3_CURRENT_POSITION = 0;
int TEXT4_CURRENT_POSITION = 0;
int TEXT5_CURRENT_POSITION = 0;
int TEXT6_CURRENT_POSITION = 0;
int TEXT7_CURRENT_POSITION = 0;
int TEXT8_CURRENT_POSITION = 0;
int TEXT9_CURRENT_POSITION = 0;
int TEXT10_CURRENT_POSITION = 0;

int TITLE_Y_POSITION = 0;
int TEXT1_Y_POSITION = 0;
int TEXT2_Y_POSITION = 0;
int TEXT3_Y_POSITION = 0;
int TEXT4_Y_POSITION = 0;
int TEXT5_Y_POSITION = 0;
int TEXT6_Y_POSITION = 0;
int TEXT7_Y_POSITION = 0;
int TEXT8_Y_POSITION = 0;
int TEXT9_Y_POSITION = 0;
int TEXT10_Y_POSITION = 0;

int TITLE_VELOCITY = 0;
int TEXT1_VELOCITY = 0;
int TEXT2_VELOCITY = 0;
int TEXT3_VELOCITY = 0;
int TEXT4_VELOCITY = 0;
int TEXT5_VELOCITY = 0;
int TEXT6_VELOCITY = 0;
int TEXT7_VELOCITY = 0;
int TEXT8_VELOCITY = 0;
int TEXT9_VELOCITY = 0;
int TEXT10_VELOCITY = 0;

int BACKGROUND_2_FADE_START = 0;
int BACKGROUND_2_FADE_END = 0;
int BACKGROUND_2_FADE_VELOCITY = 0;
int BACKGROUND_3_FADE_START = 0;
int BACKGROUND_3_FADE_END = 0;
int BACKGROUND_3_FADE_VELOCITY = 0;
int BACKGROUND_4_FADE_START = 0;
int BACKGROUND_4_FADE_END = 0;
int BACKGROUND_4_FADE_VELOCITY = 0;
int BACKGROUND_5_FADE_START = 0;
int BACKGROUND_5_FADE_END = 0;
int BACKGROUND_5_FADE_VELOCITY = 0;
int BACKGROUND_NUMBER = 1;

int BASE_SCENE_TIME = 3000;
int TOTAL_SCENE_TIME = 3000;

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str());
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Color key image
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

bool LTexture::loadFromRenderedText( TTF_Font *font, std::string textureText, SDL_Color textColor)
{
    //Get rid of preexisting texture
    free();

    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Blended( font, textureText.c_str(), textColor );
    if( textSurface == NULL )
    {
        printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
    }
    else
    {
        //Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
        if( mTexture == NULL )
        {
            printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
        }
        else
        {
            //Get image dimensions
            mWidth = textSurface->w;
            mHeight = textSurface->h;
        }

        //Get rid of old surface
        SDL_FreeSurface( textSurface );
    }

    //Return success
    return mTexture != NULL;
}

void LTexture::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		//mWidth = 0;
		//mHeight = 0;
	}
}

void LTexture::setColor( Uint8 red, Uint8 green, Uint8 blue )
{
	//Modulate texture rgb
	SDL_SetTextureColorMod( mTexture, red, green, blue );
}

void LTexture::setBlendMode( SDL_BlendMode blending )
{
	//Set blending function
	SDL_SetTextureBlendMode( mTexture, blending );
}

void LTexture::setAlpha( Uint8 alpha )
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod( mTexture, alpha );
}

void LTexture::render( int x, int y, SDL_Rect* clip )
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopy( gRenderer, mTexture, clip, &renderQuad );
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

SDL_Texture* LTexture::getTexture()
{
    return mTexture;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_FULLSCREEN_DESKTOP);
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}

				//Initialize SDL_ttf
				if( TTF_Init() == -1 )
				{
					printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadTextTexture(LTexture *textTexture, TTF_Font *font, std::string textString, SDL_Color color, bool outline)
{
    if (textString.size() > 0) {
        if (outline == true) TTF_SetFontOutline(font, 3);
        else TTF_SetFontOutline(font, 0);
        if( !textTexture->loadFromRenderedText( font, textString, color ) )
        {
            printf( "Failed to render text texture!\n" );
            return false;
        }
        textTexture->setBlendMode( SDL_BLENDMODE_BLEND );
    }
    return true;
}

int getYear()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    return (now->tm_year + 1900);
}

std::string getMonth()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    if (now->tm_mon + 1 == 1) return "January";
    else if (now->tm_mon + 1 == 2) return "February";
    else if (now->tm_mon + 1 == 3) return "March";
    else if (now->tm_mon + 1 == 4) return "April";
    else if (now->tm_mon + 1 == 5) return "May";
    else if (now->tm_mon + 1 == 6) return "June";
    else if (now->tm_mon + 1 == 7) return "July";
    else if (now->tm_mon + 1 == 8) return "August";
    else if (now->tm_mon + 1 == 9) return "September";
    else if (now->tm_mon + 1 == 10) return "October";
    else if (now->tm_mon + 1 == 11) return "November";
    else return "December";
}

std::string getDay()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    if (now->tm_mday == 1) return "1st";
    else if (now->tm_mday == 2) return "2nd";
    else if (now->tm_mday == 3) return "3rd";
    else if (now->tm_mday == 4) return "4th";
    else if (now->tm_mday == 5) return "5th";
    else if (now->tm_mday == 6) return "6th";
    else if (now->tm_mday == 7) return "7th";
    else if (now->tm_mday == 8) return "8th";
    else if (now->tm_mday == 9) return "9th";
    else if (now->tm_mday == 10) return "10th";
    else if (now->tm_mday == 11) return "11th";
    else if (now->tm_mday == 12) return "12th";
    else if (now->tm_mday == 13) return "13th";
    else if (now->tm_mday == 14) return "14th";
    else if (now->tm_mday == 15) return "15th";
    else if (now->tm_mday == 16) return "16th";
    else if (now->tm_mday == 17) return "17th";
    else if (now->tm_mday == 18) return "18th";
    else if (now->tm_mday == 19) return "19th";
    else if (now->tm_mday == 20) return "20th";
    else if (now->tm_mday == 21) return "21st";
    else if (now->tm_mday == 22) return "22nd";
    else if (now->tm_mday == 23) return "23rd";
    else if (now->tm_mday == 24) return "24th";
    else if (now->tm_mday == 25) return "25th";
    else if (now->tm_mday == 26) return "26th";
    else if (now->tm_mday == 27) return "27th";
    else if (now->tm_mday == 28) return "28th";
    else if (now->tm_mday == 29) return "29th";
    else if (now->tm_mday == 30) return "30th";
    else if (now->tm_mday == 31) return "31st";
}

std::string getDayOfWeek()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );

    int m = now->tm_mon + 1;
    int y = now->tm_year + 1900;
    int d = now->tm_mday;

    static int x[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
    y -= m < 3;
    int numberValue = ( y + y/4 - y/100 + y/400 + x[m-1] + d) % 7;

    if (numberValue == 0) return "Sunday";
    if (numberValue == 1) return "Monday";
    if (numberValue == 2) return "Tuesday";
    if (numberValue == 3) return "Wednesday";
    if (numberValue == 4) return "Thursday";
    if (numberValue == 5) return "Friday";
    if (numberValue == 6) return "Saturday";
}

bool loadMedia(Video* video)
{
	//Loading success flag
	bool success = true;

	// Reset variables
	TITLE_FADE_IN_START = 0;
    TITLE_FADE_IN_END = 0;
    TEXT1_MOVEMENT_START = 0;
    TEXT1_MOVEMENT_END = 0;
    TEXT2_MOVEMENT_START = 0;
    TEXT2_MOVEMENT_END = 0;
    TEXT3_MOVEMENT_START = 0;
    TEXT3_MOVEMENT_END = 0;
    TEXT4_MOVEMENT_START = 0;
    TEXT4_MOVEMENT_END = 0;
    TEXT5_MOVEMENT_START = 0;
    TEXT5_MOVEMENT_END = 0;
    TEXT6_MOVEMENT_START = 0;
    TEXT6_MOVEMENT_END = 0;
    TEXT7_MOVEMENT_START = 0;
    TEXT7_MOVEMENT_END = 0;
    TEXT8_MOVEMENT_START = 0;
    TEXT8_MOVEMENT_END = 0;
    TEXT9_MOVEMENT_START = 0;
    TEXT9_MOVEMENT_END = 0;
    TEXT10_MOVEMENT_START = 0;
    TEXT10_MOVEMENT_END = 0;

    TITLE_X_POSITION = 0;
    TEXT1_X_POSITION = 0;
    TEXT2_X_POSITION = 0;
    TEXT3_X_POSITION = 0;
    TEXT4_X_POSITION = 0;
    TEXT5_X_POSITION = 0;
    TEXT6_X_POSITION = 0;
    TEXT7_X_POSITION = 0;
    TEXT8_X_POSITION = 0;
    TEXT9_X_POSITION = 0;
    TEXT10_X_POSITION = 0;

    TITLE_CURRENT_POSITION = 0;
    TEXT1_CURRENT_POSITION = 0;
    TEXT2_CURRENT_POSITION = 0;
    TEXT3_CURRENT_POSITION = 0;
    TEXT4_CURRENT_POSITION = 0;
    TEXT5_CURRENT_POSITION = 0;
    TEXT6_CURRENT_POSITION = 0;
    TEXT7_CURRENT_POSITION = 0;
    TEXT8_CURRENT_POSITION = 0;
    TEXT9_CURRENT_POSITION = 0;
    TEXT10_CURRENT_POSITION = 0;

    TITLE_Y_POSITION = 0;
    TEXT1_Y_POSITION = 0;
    TEXT2_Y_POSITION = 0;
    TEXT3_Y_POSITION = 0;
    TEXT4_Y_POSITION = 0;
    TEXT5_Y_POSITION = 0;
    TEXT6_Y_POSITION = 0;
    TEXT7_Y_POSITION = 0;
    TEXT8_Y_POSITION = 0;
    TEXT9_Y_POSITION = 0;
    TEXT10_Y_POSITION = 0;

    TITLE_VELOCITY = 0;
    TEXT1_VELOCITY = 0;
    TEXT2_VELOCITY = 0;
    TEXT3_VELOCITY = 0;
    TEXT4_VELOCITY = 0;
    TEXT5_VELOCITY = 0;
    TEXT6_VELOCITY = 0;
    TEXT7_VELOCITY = 0;
    TEXT8_VELOCITY = 0;
    TEXT9_VELOCITY = 0;
    TEXT10_VELOCITY = 0;

    BACKGROUND_2_FADE_START = 0;
    BACKGROUND_2_FADE_END = 0;
    BACKGROUND_2_FADE_VELOCITY = 0;
    BACKGROUND_3_FADE_START = 0;
    BACKGROUND_3_FADE_END = 0;
    BACKGROUND_3_FADE_VELOCITY = 0;
    BACKGROUND_4_FADE_START = 0;
    BACKGROUND_4_FADE_END = 0;
    BACKGROUND_4_FADE_VELOCITY = 0;
    BACKGROUND_5_FADE_START = 0;
    BACKGROUND_5_FADE_END = 0;
    BACKGROUND_5_FADE_VELOCITY = 0;
    BACKGROUND_NUMBER = 1;

	//Load the first texture
	if( !texture1.loadFromFile(video->Getfilepath()) )
	{
		printf( "Failed to load texture!\n" );
		success = false;
	}
	else
	{
		//Set standard alpha blending
		texture1.setBlendMode( SDL_BLENDMODE_BLEND );

        //If the last character of the filename is "1," check for additional files in the sequence up to 5
        char* filepath = (char*)(video->Getfilepath().c_str());
        if(filepath[(video->Getfilepath().size()) - 5] == '1') {
            filepath[(video->Getfilepath().size()) - 5] = '2';
            texture2.loadFromFile(filepath);
            texture2.setBlendMode( SDL_BLENDMODE_BLEND );
            if (texture2.getTexture() != NULL) BACKGROUND_NUMBER +=1;

            filepath[(video->Getfilepath().size()) - 5] = '3';
            texture3.loadFromFile(filepath);
            texture3.setBlendMode( SDL_BLENDMODE_BLEND );
            if (texture3.getTexture() != NULL) BACKGROUND_NUMBER +=1;

            filepath[(video->Getfilepath().size()) - 5] = '4';
            texture4.loadFromFile(filepath);
            texture4.setBlendMode( SDL_BLENDMODE_BLEND );
            if (texture4.getTexture() != NULL) BACKGROUND_NUMBER +=1;

            filepath[(video->Getfilepath().size()) - 5] = '5';
            texture5.loadFromFile(filepath);
            texture5.setBlendMode( SDL_BLENDMODE_BLEND );
            if (texture5.getTexture() != NULL) BACKGROUND_NUMBER +=1;

            filepath[(video->Getfilepath().size()) - 5] = '1';
        } else {
            BACKGROUND_NUMBER = 1;
            texture2.loadFromFile(filepath);
            texture3.loadFromFile(filepath);
            texture4.loadFromFile(filepath);
            texture5.loadFromFile(filepath);
        }

        //Determine font sizes as a function of screen size and text length.
        int TITLE_FONT_SIZE = 72;
        int FONT_SIZE = 38;

        const int titleFactorNumerator = 9;
        const int titleFactorDenominator = 5;
        const int mainFactorNumerator = 11;
        const int mainFactorDenominator = 16;

        if (video->getVideoType() == TYPE_CUST_TEXT) {
            CustomTextVideo* customTextVideo = (CustomTextVideo*)video;
            TITLE_FONT_SIZE = (SCREEN_WIDTH / titleFactorDenominator) / (customTextVideo->Gettitle().size()) * titleFactorNumerator;
            if (TITLE_FONT_SIZE > 125) TITLE_FONT_SIZE = 125;

            int maxTextSize = customTextVideo->Gettext1().size();
            if (customTextVideo->Gettext2().size() > maxTextSize) maxTextSize = customTextVideo->Gettext2().size();
            if (customTextVideo->Gettext3().size() > maxTextSize) maxTextSize = customTextVideo->Gettext3().size();
            if (customTextVideo->Gettext4().size() > maxTextSize) maxTextSize = customTextVideo->Gettext4().size();
            if (customTextVideo->Gettext5().size() > maxTextSize) maxTextSize = customTextVideo->Gettext5().size();
            if (customTextVideo->Gettext6().size() > maxTextSize) maxTextSize = customTextVideo->Gettext6().size();
            if (customTextVideo->Gettext7().size() > maxTextSize) maxTextSize = customTextVideo->Gettext7().size();
            if (customTextVideo->Gettext8().size() > maxTextSize) maxTextSize = customTextVideo->Gettext8().size();
            if (customTextVideo->Gettext9().size() > maxTextSize) maxTextSize = customTextVideo->Gettext9().size();
            if (customTextVideo->Gettext10().size() > maxTextSize) maxTextSize = customTextVideo->Gettext10().size();

            FONT_SIZE = (SCREEN_WIDTH / mainFactorDenominator) / (maxTextSize * .95) * mainFactorNumerator;
        }
        else if (video->getVideoType() == TYPE_TEMP) {
            TemperatureVideo* tempVideo = (TemperatureVideo*)video;
            TITLE_FONT_SIZE = (SCREEN_WIDTH)/3;
            FONT_SIZE = (SCREEN_WIDTH)/15;
        }
        else if (video->getVideoType() == TYPE_DATE) {
            DateVideo* dateVideo = (DateVideo*)video;
            TITLE_FONT_SIZE = (SCREEN_WIDTH)/5;
            FONT_SIZE = (SCREEN_WIDTH)/15;
        }

        //Open the font
        titleFont = TTF_OpenFont( (char*)titleFontPath.c_str(), TITLE_FONT_SIZE );
        if( titleFont == NULL )
        {
            printf( "Failed to load title font! SDL_ttf Error: %s\n", TTF_GetError() );
            success = false;
        }
        else
        {
            gFont = TTF_OpenFont( (char*)mainFontPath.c_str(), FONT_SIZE );
            if( gFont == NULL )
            {
                printf( "Failed to load main font! SDL_ttf Error: %s\n", TTF_GetError() );
                success = false;
            }
            else {
                SDL_Color OUTLINE_COLOR = {0,0,0,0};
                SDL_Color MAIN_FONT_COLOR = {255,255,255,0};

                //Render text
                if (video->getVideoType() == TYPE_CUST_TEXT) {
                    CustomTextVideo* customTextVideo = (CustomTextVideo*)video;
                    success = loadTextTexture(&gTitleOutlineTexture, titleFont, customTextVideo->Gettitle(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTitleTexture, titleFont, customTextVideo->Gettitle(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture1, gFont, customTextVideo->Gettext1(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture1, gFont, customTextVideo->Gettext1(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture2, gFont, customTextVideo->Gettext2(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture2, gFont, customTextVideo->Gettext2(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture3, gFont, customTextVideo->Gettext3(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture3, gFont, customTextVideo->Gettext3(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture4, gFont, customTextVideo->Gettext4(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture4, gFont, customTextVideo->Gettext4(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture5, gFont, customTextVideo->Gettext5(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture5, gFont, customTextVideo->Gettext5(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture6, gFont, customTextVideo->Gettext6(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture6, gFont, customTextVideo->Gettext6(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture7, gFont, customTextVideo->Gettext7(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture7, gFont, customTextVideo->Gettext7(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture8, gFont, customTextVideo->Gettext8(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture8, gFont, customTextVideo->Gettext8(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture9, gFont, customTextVideo->Gettext9(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture9, gFont, customTextVideo->Gettext9(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextOutlineTexture10, gFont, customTextVideo->Gettext10(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTexture10, gFont, customTextVideo->Gettext10(), MAIN_FONT_COLOR, false);
                }
                else if (video->getVideoType() == TYPE_TEMP) {
                    TemperatureVideo* tempVideo = (TemperatureVideo*)video;
                    success = loadTextTexture(&gTextTemperatureOutline1, gFont, "The temperature inside is", OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTemperature1, gFont, "The temperature inside is", MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextTemperatureOutline2, titleFont, tempVideo->Gettemperature(), OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTemperature2, titleFont, tempVideo->Gettemperature(), MAIN_FONT_COLOR, false);
                    success = success && loadTextTexture(&gTextTemperatureOutline3, gFont, "Come on in!", OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextTemperature3, gFont, "Come on in!", MAIN_FONT_COLOR, false);
                }
                else if (video->getVideoType() == TYPE_DATE) {
                    DateVideo* dateVideo = (DateVideo*)video;
                    success = loadTextTexture(&gTextDateOutline1, gFont, "Today is", OUTLINE_COLOR, true);
                    success = success && loadTextTexture(&gTextDate1, gFont, "Today is", MAIN_FONT_COLOR, false);

                    time_t t = time(0);   // get time now
                    struct tm * now = localtime( & t );
                    if (now->tm_mday != tomorrow_day) {
                        dayNumber = getDay();
                        dayOfWeek = getDayOfWeek();
                        month = getMonth();
                        year = getYear();
                        yearString = static_cast<std::ostringstream*>( &(std::ostringstream() << year) )->str();

                        if (dateVideo->Getday() == true && dateVideo->Getmonth() == true) {
                            finalDateString = dayOfWeek + ", " + month + " " + dayNumber;
                        } else if (dateVideo->Getday() == false && dateVideo->Getmonth() == true) {
                            finalDateString = month + " " + dayNumber;
                        } else if (dateVideo->Getday() == true && dateVideo->Getmonth() == false) {
                            finalDateString = dayOfWeek;
                        }

                        tomorrow_day = now->tm_mday;
                    }

                    if (finalDateString.size() > 0) {
                        success = success && loadTextTexture(&gTextDateOutline2, gFont, finalDateString, OUTLINE_COLOR, true);
                        success = success && loadTextTexture(&gTextDate2, gFont, finalDateString, MAIN_FONT_COLOR, false);
                    }

                    if (dateVideo->Getyear() == true) {
                        if (yearString.size() > 0) {
                            success = success && loadTextTexture(&gTextDateOutline3, titleFont, yearString, OUTLINE_COLOR, true);
                            success = success && loadTextTexture(&gTextDate3, titleFont, yearString, MAIN_FONT_COLOR, false);
                        }
                    }
                }
            }
        }
	}
	return success;
}

void close()
{
	//Free loaded images
	texture1.free();
	texture2.free();
	texture3.free();
	texture4.free();
	texture5.free();

	//Free global text
	gTitleOutlineTexture.free();
	gTitleTexture.free();
	gTextTexture1.free();
	gTextTexture2.free();
	gTextTexture3.free();
	gTextTexture4.free();
	gTextTexture5.free();
	gTextTexture6.free();
	gTextTexture7.free();
	gTextTexture8.free();
	gTextTexture9.free();
	gTextTexture10.free();
	gTextOutlineTexture1.free();
	gTextOutlineTexture2.free();
	gTextOutlineTexture3.free();
	gTextOutlineTexture4.free();
	gTextOutlineTexture5.free();
	gTextOutlineTexture6.free();
	gTextOutlineTexture7.free();
	gTextOutlineTexture8.free();
	gTextOutlineTexture9.free();
	gTextOutlineTexture10.free();

    gTextTemperature1.free();
    gTextTemperature2.free();
    gTextTemperature3.free();
	gTextTemperatureOutline1.free();
	gTextTemperatureOutline2.free();
	gTextTemperatureOutline3.free();

    gTextDate1.free();
    gTextDate2.free();
    gTextDate3.free();
	gTextDateOutline1.free();
	gTextDateOutline2.free();
	gTextDateOutline3.free();

	//Free fonts
    TTF_CloseFont( titleFont );
    titleFont = NULL;
    TTF_CloseFont( gFont );
    gFont = NULL;
}

void quitSDL()
{
    //Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
	TTF_Quit();
}

int showVisual(std::vector<std::unique_ptr<Video>> *schedule, bool random)
{
    Video* video;
    int event_cnt = 0;
    int background2AlphaLevel = 0;
    int background3AlphaLevel = 0;
    int background4AlphaLevel = 0;
    int background5AlphaLevel = 0;
    int alphaLevel = 0;
    int next_number = -1;
    int prev_number = -1;

    timer = 0;

	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
        //Main loop flag
        bool quit = false;

        //Event handler
        SDL_Event e;

        //Modulation component
        Uint8 a = 255;

        //While application is running
        while( !quit )
        {
            // If the timer is at 0, load the next Video object.
            if (timer == 0) {
                // Determine the next video to play.
                if (random == true) {
                    next_number = rand() % (schedule->size());
                    if (prev_number == next_number) next_number = (next_number + 1)%(schedule->size());
                }
                else {
                    next_number = (next_number + 1) % (schedule->size());
                }
                prev_number = next_number;
                video = (schedule->at(next_number)).get();

                // Free resources before starting the next video.
                close();

                if ( !loadMedia(video) )
                {
                    printf( "Failed to load media!\n" );
                    quit = true;
                }

                if (video->getVideoType() == TYPE_CUST_TEXT) {
                    CustomTextVideo* customTextVideo = (CustomTextVideo*)video;

                    // At the very least, there will be two events (title fade in start, and title fade in finish),
                    // so we set the count for timed events to 2 by default.
                    event_cnt = 2;
                    if (customTextVideo->Gettext1().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext2().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext3().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext4().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext5().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext6().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext7().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext8().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext9().size() > 0)  event_cnt += 2;
                    if (customTextVideo->Gettext10().size() > 0) event_cnt += 2;

                    TOTAL_SCENE_TIME = BASE_SCENE_TIME + ((event_cnt)*30);
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR) TOTAL_SCENE_TIME = (TOTAL_SCENE_TIME * 2 / 3);

                    TITLE_FADE_IN_START = (TOTAL_SCENE_TIME/(event_cnt+1))*1;
                    TITLE_FADE_IN_END = (TOTAL_SCENE_TIME/(event_cnt+1))*2;
                    TEXT1_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*3;
                    TEXT1_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*4;
                    TEXT2_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*5;
                    TEXT2_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*6;
                    TEXT3_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*7;
                    TEXT3_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*8;
                    TEXT4_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*9;
                    TEXT4_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*10;
                    TEXT5_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*11;
                    TEXT5_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*12;
                    TEXT6_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*13;
                    TEXT6_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*14;
                    TEXT7_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*15;
                    TEXT7_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*16;
                    TEXT8_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*17;
                    TEXT8_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*18;
                    TEXT9_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*19;
                    TEXT9_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*20;
                    TEXT10_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt+1))*21;
                    TEXT10_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt+1))*22;

                    TITLE_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*1;
                    TEXT1_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*3;
                    TEXT2_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*4;
                    TEXT3_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*5;
                    TEXT4_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*6;
                    TEXT5_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*7;
                    TEXT6_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*8;
                    TEXT7_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*9;
                    TEXT8_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*10;
                    TEXT9_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*11;
                    TEXT10_Y_POSITION = (SCREEN_HEIGHT/((event_cnt+6)/2))*12;

                    TITLE_X_POSITION = (SCREEN_WIDTH - gTitleTexture.getWidth()) / 2;
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR) {
                        TITLE_CURRENT_POSITION = TITLE_Y_POSITION;
                    }
                    else {
                        TITLE_CURRENT_POSITION = 0 - (gTitleTexture.getHeight());
                    }

                    if (customTextVideo->GetalignDirection() == ICD_CUST_RADIO_ALIGN_LEFT) {
                        TEXT1_X_POSITION = 15;
                        TEXT2_X_POSITION = 15;
                        TEXT3_X_POSITION = 15;
                        TEXT4_X_POSITION = 15;
                        TEXT5_X_POSITION = 15;
                        TEXT6_X_POSITION = 15;
                        TEXT7_X_POSITION = 15;
                        TEXT8_X_POSITION = 15;
                        TEXT9_X_POSITION = 15;
                        TEXT10_X_POSITION = 15;
                    } else if (customTextVideo->GetalignDirection() == ICD_CUST_RADIO_ALIGN_RIGHT) {
                        TEXT1_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture1.getWidth();
                        TEXT2_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture2.getWidth();
                        TEXT3_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture3.getWidth();
                        TEXT4_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture4.getWidth();
                        TEXT5_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture5.getWidth();
                        TEXT6_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture6.getWidth();
                        TEXT7_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture7.getWidth();
                        TEXT8_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture8.getWidth();
                        TEXT9_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture9.getWidth();
                        TEXT10_X_POSITION = SCREEN_WIDTH - 15 - gTextOutlineTexture10.getWidth();
                    }
                    else if (customTextVideo->GetalignDirection() == ICD_CUST_RADIO_ALIGN_CENTER) {
                        TEXT1_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture1.getWidth()/2);
                        TEXT2_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture2.getWidth()/2);
                        TEXT3_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture3.getWidth()/2);
                        TEXT4_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture4.getWidth()/2);
                        TEXT5_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture5.getWidth()/2);
                        TEXT6_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture6.getWidth()/2);
                        TEXT7_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture7.getWidth()/2);
                        TEXT8_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture8.getWidth()/2);
                        TEXT9_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture9.getWidth()/2);
                        TEXT10_X_POSITION = (SCREEN_WIDTH/2) - (gTextOutlineTexture10.getWidth()/2);
                    }


                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        TEXT1_CURRENT_POSITION = 0 - gTextOutlineTexture1.getWidth();
                        TEXT2_CURRENT_POSITION = 0 - gTextOutlineTexture2.getWidth();
                        TEXT3_CURRENT_POSITION = 0 - gTextOutlineTexture3.getWidth();
                        TEXT4_CURRENT_POSITION = 0 - gTextOutlineTexture4.getWidth();
                        TEXT5_CURRENT_POSITION = 0 - gTextOutlineTexture5.getWidth();
                        TEXT6_CURRENT_POSITION = 0 - gTextOutlineTexture6.getWidth();
                        TEXT7_CURRENT_POSITION = 0 - gTextOutlineTexture7.getWidth();
                        TEXT8_CURRENT_POSITION = 0 - gTextOutlineTexture8.getWidth();
                        TEXT9_CURRENT_POSITION = 0 - gTextOutlineTexture9.getWidth();
                        TEXT10_CURRENT_POSITION = 0 - gTextOutlineTexture10.getWidth();
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        TEXT1_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT2_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT3_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT4_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT5_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT6_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT7_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT8_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT9_CURRENT_POSITION = SCREEN_WIDTH + 3;
                        TEXT10_CURRENT_POSITION = SCREEN_WIDTH + 3;
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR) {
                        TEXT1_CURRENT_POSITION = TEXT1_X_POSITION;
                        TEXT2_CURRENT_POSITION = TEXT2_X_POSITION;
                        TEXT3_CURRENT_POSITION = TEXT3_X_POSITION;
                        TEXT4_CURRENT_POSITION = TEXT4_X_POSITION;
                        TEXT5_CURRENT_POSITION = TEXT5_X_POSITION;
                        TEXT6_CURRENT_POSITION = TEXT6_X_POSITION;
                        TEXT7_CURRENT_POSITION = TEXT7_X_POSITION;
                        TEXT8_CURRENT_POSITION = TEXT8_X_POSITION;
                        TEXT9_CURRENT_POSITION = TEXT9_X_POSITION;
                        TEXT10_CURRENT_POSITION = TEXT10_X_POSITION;
                    }

                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT || ICD_CUST_RADIO_SLIDE_RIGHT) {
                        TITLE_VELOCITY = (TITLE_Y_POSITION)/(TITLE_FADE_IN_END-TITLE_FADE_IN_START);

                        if(TITLE_VELOCITY == 0) TITLE_VELOCITY = 1;
                        TEXT1_VELOCITY = (TEXT1_X_POSITION - TEXT1_CURRENT_POSITION)/(TEXT1_MOVEMENT_END - TEXT1_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT1_VELOCITY < 2) TEXT1_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT1_VELOCITY > -2) TEXT1_VELOCITY = -2;
                        TEXT2_VELOCITY = (TEXT2_X_POSITION - TEXT2_CURRENT_POSITION)/(TEXT2_MOVEMENT_END - TEXT2_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT2_VELOCITY < 2) TEXT2_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT2_VELOCITY > -2) TEXT2_VELOCITY = -2;
                        TEXT3_VELOCITY = (TEXT3_X_POSITION - TEXT3_CURRENT_POSITION)/(TEXT3_MOVEMENT_END - TEXT3_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT3_VELOCITY < 2) TEXT3_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT3_VELOCITY > -2) TEXT3_VELOCITY = -2;
                        TEXT4_VELOCITY = (TEXT4_X_POSITION - TEXT4_CURRENT_POSITION)/(TEXT4_MOVEMENT_END - TEXT4_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT4_VELOCITY < 2) TEXT4_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT4_VELOCITY > -2) TEXT4_VELOCITY = -2;
                        TEXT5_VELOCITY = (TEXT5_X_POSITION - TEXT5_CURRENT_POSITION)/(TEXT5_MOVEMENT_END - TEXT5_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT5_VELOCITY < 2) TEXT5_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT5_VELOCITY > -2) TEXT5_VELOCITY = -2;
                        TEXT6_VELOCITY = (TEXT6_X_POSITION - TEXT6_CURRENT_POSITION)/(TEXT6_MOVEMENT_END - TEXT6_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT6_VELOCITY < 2) TEXT6_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT6_VELOCITY > -2) TEXT6_VELOCITY = -2;
                        TEXT7_VELOCITY = (TEXT7_X_POSITION - TEXT7_CURRENT_POSITION)/(TEXT7_MOVEMENT_END - TEXT7_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT7_VELOCITY < 2) TEXT7_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT7_VELOCITY > -2) TEXT7_VELOCITY = -2;
                        TEXT8_VELOCITY = (TEXT8_X_POSITION - TEXT8_CURRENT_POSITION)/(TEXT8_MOVEMENT_END - TEXT8_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT8_VELOCITY < 2) TEXT8_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT8_VELOCITY > -2) TEXT8_VELOCITY = -2;
                        TEXT9_VELOCITY = (TEXT9_X_POSITION - TEXT9_CURRENT_POSITION)/(TEXT9_MOVEMENT_END - TEXT9_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT9_VELOCITY < 2) TEXT9_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT9_VELOCITY > -2) TEXT9_VELOCITY = -2;
                        TEXT10_VELOCITY = (TEXT10_X_POSITION - TEXT10_CURRENT_POSITION)/(TEXT10_MOVEMENT_END - TEXT10_MOVEMENT_START);
                        if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT && TEXT10_VELOCITY < 2) TEXT10_VELOCITY = 2;
                        else if(customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT && TEXT10_VELOCITY > -2) TEXT10_VELOCITY = -2;
                    }
                }
                else if (video->getVideoType() == TYPE_TEMP) {
                    TemperatureVideo* temperatureVideo = (TemperatureVideo*)video;

                    TOTAL_SCENE_TIME = BASE_SCENE_TIME/3;
                    event_cnt = 12;

                    TEXT1_X_POSITION = (SCREEN_WIDTH/2)-(gTextTemperature1.getWidth()/2);
                    TEXT1_Y_POSITION = (SCREEN_HEIGHT/10);

                    TEXT2_X_POSITION = (SCREEN_WIDTH/2)-(gTextTemperature2.getWidth()/2);
                    TEXT2_Y_POSITION = (SCREEN_HEIGHT/2)-(gTextTemperature2.getHeight()/2);

                    TEXT3_X_POSITION = (SCREEN_WIDTH/2)-(gTextTemperature3.getWidth()/2);
                    TEXT3_Y_POSITION = (SCREEN_HEIGHT*8)/10;

                    TEXT1_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*2;
                    TEXT1_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*3;
                    TEXT2_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*5;
                    TEXT2_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*6;
                    TEXT3_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*8;
                    TEXT3_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*9;
                }
                else if (video->getVideoType() == TYPE_DATE) {
                    DateVideo* dateVideo = (DateVideo*)video;

                    TOTAL_SCENE_TIME = BASE_SCENE_TIME/3;
                    event_cnt = 8;

                    if (dateVideo->Getday() == true || dateVideo->Getmonth() == true) event_cnt += 2;
                    if (dateVideo->Getyear() == true) event_cnt += 2;

                    if (dateVideo->Getyear() == true) {
                        TEXT1_X_POSITION = (SCREEN_WIDTH/2)-(gTextDate1.getWidth()/2);
                        TEXT1_Y_POSITION = (SCREEN_HEIGHT/10);
                        TEXT2_X_POSITION = (SCREEN_WIDTH/2)-(gTextDate2.getWidth()/2);
                        TEXT2_Y_POSITION = (SCREEN_HEIGHT*3)/10;
                        TEXT3_X_POSITION = (SCREEN_WIDTH/2)-(gTextDate3.getWidth()/2);
                        TEXT3_Y_POSITION = (SCREEN_HEIGHT*5)/10;
                    } else {
                        TEXT1_X_POSITION = (SCREEN_WIDTH/2)-(gTextDate1.getWidth()/2);
                        TEXT1_Y_POSITION = (SCREEN_HEIGHT*3)/10;
                        TEXT2_X_POSITION = (SCREEN_WIDTH/2)-(gTextDate2.getWidth()/2);
                        TEXT2_Y_POSITION = (SCREEN_HEIGHT*5)/10;
                    }

                    TEXT1_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*2;
                    TEXT1_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*3;
                    TEXT2_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*5;
                    TEXT2_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*6;
                    TEXT3_MOVEMENT_START = (TOTAL_SCENE_TIME/(event_cnt))*8;
                    TEXT3_MOVEMENT_END = (TOTAL_SCENE_TIME/(event_cnt))*9;
                }

                BACKGROUND_2_FADE_START = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*1;
                BACKGROUND_2_FADE_END = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*3;
                BACKGROUND_3_FADE_START = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*4;
                BACKGROUND_3_FADE_END = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*5;
                BACKGROUND_4_FADE_START = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*6;
                BACKGROUND_4_FADE_END = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*7;
                BACKGROUND_5_FADE_START = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*8;
                BACKGROUND_5_FADE_END = (TOTAL_SCENE_TIME/(BACKGROUND_NUMBER*2))*9;

                BACKGROUND_2_FADE_VELOCITY = 255/(BACKGROUND_2_FADE_END - BACKGROUND_2_FADE_START);
                if (BACKGROUND_2_FADE_VELOCITY == 0) BACKGROUND_2_FADE_VELOCITY += 1;
                BACKGROUND_3_FADE_VELOCITY = 255/(BACKGROUND_3_FADE_END - BACKGROUND_3_FADE_START);
                if (BACKGROUND_3_FADE_VELOCITY == 0) BACKGROUND_3_FADE_VELOCITY += 1;
                BACKGROUND_4_FADE_VELOCITY = 255/(BACKGROUND_4_FADE_END - BACKGROUND_4_FADE_START);
                if (BACKGROUND_4_FADE_VELOCITY == 0) BACKGROUND_4_FADE_VELOCITY += 1;
                BACKGROUND_5_FADE_VELOCITY = 255/(BACKGROUND_5_FADE_END - BACKGROUND_5_FADE_START);
                if (BACKGROUND_5_FADE_VELOCITY == 0) BACKGROUND_5_FADE_VELOCITY += 1;
            }

            //Handle events on queue
            while( SDL_PollEvent( &e ) != 0 )
            {
                //Handle key presses
                if( e.type == SDL_KEYDOWN )
                {
                    //Increase alpha on w
                    if( e.key.keysym.sym == SDLK_ESCAPE )
                    {
                        quit = true;
                    }
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
            SDL_RenderClear( gRenderer );

            //Render backgrounds
            texture1.render( 0, 0 );
            if (timer == 0) {
                background2AlphaLevel=0;
                background3AlphaLevel=0;
                background4AlphaLevel=0;
                background5AlphaLevel=0;
            }
            if (timer >= BACKGROUND_2_FADE_START) {
                background2AlphaLevel = background2AlphaLevel + BACKGROUND_2_FADE_VELOCITY;
                if (background2AlphaLevel >=255 ) background2AlphaLevel = 255;
                texture2.setAlpha(background2AlphaLevel);
                texture2.render(0,0);
            }
            if (timer >= BACKGROUND_3_FADE_START) {
                background3AlphaLevel = background3AlphaLevel + BACKGROUND_3_FADE_VELOCITY;
                if (background3AlphaLevel >=255 ) background3AlphaLevel = 255;
                texture3.setAlpha(background3AlphaLevel);
                texture3.render(0,0);
            }
            if (timer >= BACKGROUND_4_FADE_START) {
                background4AlphaLevel = background4AlphaLevel + BACKGROUND_4_FADE_VELOCITY;
                if (background4AlphaLevel >=255 ) background4AlphaLevel = 255;
                texture4.setAlpha(background4AlphaLevel);
                texture4.render(0,0);
            }
            if (timer >= BACKGROUND_5_FADE_START) {
                background5AlphaLevel = background5AlphaLevel + BACKGROUND_5_FADE_VELOCITY;
                if (background5AlphaLevel >=255 ) background5AlphaLevel = 255;
                texture5.setAlpha(background5AlphaLevel);
                texture5.render(0,0);
            }


            //Render text
            if (video->getVideoType() == TYPE_CUST_TEXT) {
                CustomTextVideo* customTextVideo = (CustomTextVideo*)video;

                if (timer == 0 && customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR) {
                    gTextOutlineTexture1.setAlpha(0);
                    gTextTexture1.setAlpha(0);
                    gTextOutlineTexture2.setAlpha(0);
                    gTextTexture2.setAlpha(0);
                    gTextOutlineTexture3.setAlpha(0);
                    gTextTexture3.setAlpha(0);
                    gTextOutlineTexture4.setAlpha(0);
                    gTextTexture4.setAlpha(0);
                    gTextOutlineTexture5.setAlpha(0);
                    gTextTexture5.setAlpha(0);
                    gTextOutlineTexture6.setAlpha(0);
                    gTextTexture6.setAlpha(0);
                    gTextOutlineTexture7.setAlpha(0);
                    gTextTexture7.setAlpha(0);
                    gTextOutlineTexture8.setAlpha(0);
                    gTextTexture8.setAlpha(0);
                    gTextOutlineTexture9.setAlpha(0);
                    gTextTexture9.setAlpha(0);
                    gTextOutlineTexture10.setAlpha(0);
                    gTextTexture10.setAlpha(0);
                }

                if (timer >= TITLE_FADE_IN_START) {
                    if (TITLE_CURRENT_POSITION < TITLE_Y_POSITION) {
                        TITLE_CURRENT_POSITION = TITLE_CURRENT_POSITION + TITLE_VELOCITY;
                    } else {
                        TITLE_CURRENT_POSITION = TITLE_Y_POSITION;
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer < TEXT1_MOVEMENT_START) {
                    if (timer <= TITLE_FADE_IN_START) alphaLevel = 0;
                    else if (timer >= TITLE_FADE_IN_START && timer <= TITLE_FADE_IN_END && alphaLevel < 255) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TITLE_FADE_IN_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTitleOutlineTexture.setAlpha(alphaLevel);
                    gTitleTexture.setAlpha(alphaLevel);
                }
                gTitleOutlineTexture.render(TITLE_X_POSITION-3, TITLE_CURRENT_POSITION-3);
                gTitleTexture.render(TITLE_X_POSITION, TITLE_CURRENT_POSITION);

                if (timer >= TEXT1_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT1_CURRENT_POSITION < TEXT1_X_POSITION) {
                            TEXT1_CURRENT_POSITION = TEXT1_CURRENT_POSITION + TEXT1_VELOCITY;
                        } else {
                            TEXT1_CURRENT_POSITION = TEXT1_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT1_CURRENT_POSITION > TEXT1_X_POSITION) {
                            TEXT1_CURRENT_POSITION = TEXT1_CURRENT_POSITION + TEXT1_VELOCITY;
                        } else {
                            TEXT1_CURRENT_POSITION = TEXT1_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TITLE_FADE_IN_END && timer < TEXT2_MOVEMENT_START) {
                    if (timer < TEXT1_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT1_MOVEMENT_START && timer <= TEXT1_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT1_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture1.setAlpha(alphaLevel);
                    gTextTexture1.setAlpha(alphaLevel);
                }
                gTextOutlineTexture1.render(TEXT1_CURRENT_POSITION-3, TEXT1_Y_POSITION-3);
                gTextTexture1.render(TEXT1_CURRENT_POSITION, TEXT1_Y_POSITION);

                if (timer >= TEXT2_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT2_CURRENT_POSITION < TEXT2_X_POSITION) {
                            TEXT2_CURRENT_POSITION = TEXT2_CURRENT_POSITION + TEXT2_VELOCITY;
                        } else {
                            TEXT2_CURRENT_POSITION = TEXT2_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT2_CURRENT_POSITION > TEXT2_X_POSITION) {
                            TEXT2_CURRENT_POSITION = TEXT2_CURRENT_POSITION + TEXT2_VELOCITY;
                        } else {
                            TEXT2_CURRENT_POSITION = TEXT2_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT1_MOVEMENT_END && timer < TEXT3_MOVEMENT_START) {
                    if (timer < TEXT2_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT2_MOVEMENT_START && timer <= TEXT2_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT2_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture2.setAlpha(alphaLevel);
                    gTextTexture2.setAlpha(alphaLevel);
                }
                gTextOutlineTexture2.render(TEXT2_CURRENT_POSITION-3, TEXT2_Y_POSITION-3);
                gTextTexture2.render(TEXT2_CURRENT_POSITION, TEXT2_Y_POSITION);

                if (timer >= TEXT3_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT3_CURRENT_POSITION < TEXT3_X_POSITION) {
                            TEXT3_CURRENT_POSITION = TEXT3_CURRENT_POSITION + TEXT3_VELOCITY;
                        } else {
                            TEXT3_CURRENT_POSITION = TEXT3_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT3_CURRENT_POSITION > TEXT3_X_POSITION) {
                            TEXT3_CURRENT_POSITION = TEXT3_CURRENT_POSITION + TEXT3_VELOCITY;
                        } else {
                            TEXT3_CURRENT_POSITION = TEXT3_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT2_MOVEMENT_END && timer < TEXT4_MOVEMENT_START) {
                    if (timer < TEXT3_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT3_MOVEMENT_START && timer <= TEXT3_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT3_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture3.setAlpha(alphaLevel);
                    gTextTexture3.setAlpha(alphaLevel);
                }
                gTextOutlineTexture3.render(TEXT3_CURRENT_POSITION-3, TEXT3_Y_POSITION-3);
                gTextTexture3.render(TEXT3_CURRENT_POSITION, TEXT3_Y_POSITION);

                if (timer >= TEXT4_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT4_CURRENT_POSITION < TEXT4_X_POSITION) {
                            TEXT4_CURRENT_POSITION = TEXT4_CURRENT_POSITION + TEXT4_VELOCITY;
                        } else {
                            TEXT4_CURRENT_POSITION = TEXT4_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT4_CURRENT_POSITION > TEXT4_X_POSITION) {
                            TEXT4_CURRENT_POSITION = TEXT4_CURRENT_POSITION + TEXT4_VELOCITY;
                        } else {
                            TEXT4_CURRENT_POSITION = TEXT4_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT3_MOVEMENT_END && timer < TEXT5_MOVEMENT_START) {
                    if (timer < TEXT4_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT4_MOVEMENT_START && timer <= TEXT4_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT4_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture4.setAlpha(alphaLevel);
                    gTextTexture4.setAlpha(alphaLevel);
                }
                gTextOutlineTexture4.render(TEXT4_CURRENT_POSITION-3, TEXT4_Y_POSITION-3);
                gTextTexture4.render(TEXT4_CURRENT_POSITION, TEXT4_Y_POSITION);

                if (timer >= TEXT5_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT5_CURRENT_POSITION < TEXT5_X_POSITION) {
                            TEXT5_CURRENT_POSITION = TEXT5_CURRENT_POSITION + TEXT5_VELOCITY;
                        } else {
                            TEXT5_CURRENT_POSITION = TEXT5_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT5_CURRENT_POSITION > TEXT5_X_POSITION) {
                            TEXT5_CURRENT_POSITION = TEXT5_CURRENT_POSITION + TEXT5_VELOCITY;
                        } else {
                            TEXT5_CURRENT_POSITION = TEXT5_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT4_MOVEMENT_END && timer < TEXT6_MOVEMENT_START) {
                    if (timer < TEXT5_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT5_MOVEMENT_START && timer <= TEXT5_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT5_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture5.setAlpha(alphaLevel);
                    gTextTexture5.setAlpha(alphaLevel);
                }
                gTextOutlineTexture5.render(TEXT5_CURRENT_POSITION-3, TEXT5_Y_POSITION-3);
                gTextTexture5.render(TEXT5_CURRENT_POSITION, TEXT5_Y_POSITION);

                if (timer >= TEXT6_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT6_CURRENT_POSITION < TEXT6_X_POSITION) {
                            TEXT6_CURRENT_POSITION = TEXT6_CURRENT_POSITION + TEXT6_VELOCITY;
                        } else {
                            TEXT6_CURRENT_POSITION = TEXT6_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT6_CURRENT_POSITION > TEXT6_X_POSITION) {
                            TEXT6_CURRENT_POSITION = TEXT6_CURRENT_POSITION + TEXT6_VELOCITY;
                        } else {
                            TEXT6_CURRENT_POSITION = TEXT6_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT5_MOVEMENT_END && timer < TEXT7_MOVEMENT_START) {
                    if (timer < TEXT6_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT6_MOVEMENT_START && timer <= TEXT6_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT6_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture6.setAlpha(alphaLevel);
                    gTextTexture6.setAlpha(alphaLevel);
                }
                gTextOutlineTexture6.render(TEXT6_CURRENT_POSITION-3, TEXT6_Y_POSITION-3);
                gTextTexture6.render(TEXT6_CURRENT_POSITION, TEXT6_Y_POSITION);

                if (timer >= TEXT7_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT7_CURRENT_POSITION < TEXT7_X_POSITION) {
                            TEXT7_CURRENT_POSITION = TEXT7_CURRENT_POSITION + TEXT7_VELOCITY;
                        } else {
                            TEXT7_CURRENT_POSITION = TEXT7_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT7_CURRENT_POSITION > TEXT7_X_POSITION) {
                            TEXT7_CURRENT_POSITION = TEXT7_CURRENT_POSITION + TEXT7_VELOCITY;
                        } else {
                            TEXT7_CURRENT_POSITION = TEXT7_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT6_MOVEMENT_END && timer < TEXT8_MOVEMENT_START) {
                    if (timer < TEXT7_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT7_MOVEMENT_START && timer <= TEXT7_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT7_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture7.setAlpha(alphaLevel);
                    gTextTexture7.setAlpha(alphaLevel);
                }
                gTextOutlineTexture7.render(TEXT7_CURRENT_POSITION-3, TEXT7_Y_POSITION-3);
                gTextTexture7.render(TEXT7_CURRENT_POSITION, TEXT7_Y_POSITION);

                if (timer >= TEXT8_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT8_CURRENT_POSITION < TEXT8_X_POSITION) {
                            TEXT8_CURRENT_POSITION = TEXT8_CURRENT_POSITION + TEXT8_VELOCITY;
                        } else {
                            TEXT8_CURRENT_POSITION = TEXT8_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT8_CURRENT_POSITION > TEXT8_X_POSITION) {
                            TEXT8_CURRENT_POSITION = TEXT8_CURRENT_POSITION + TEXT8_VELOCITY;
                        } else {
                            TEXT8_CURRENT_POSITION = TEXT8_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT7_MOVEMENT_END && timer < TEXT9_MOVEMENT_START) {
                    if (timer < TEXT8_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT8_MOVEMENT_START && timer <= TEXT8_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT8_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture8.setAlpha(alphaLevel);
                    gTextTexture8.setAlpha(alphaLevel);
                }
                gTextOutlineTexture8.render(TEXT8_CURRENT_POSITION-3, TEXT8_Y_POSITION-3);
                gTextTexture8.render(TEXT8_CURRENT_POSITION, TEXT8_Y_POSITION);

                if (timer >= TEXT9_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT9_CURRENT_POSITION < TEXT9_X_POSITION) {
                            TEXT9_CURRENT_POSITION = TEXT9_CURRENT_POSITION + TEXT9_VELOCITY;
                        } else {
                            TEXT9_CURRENT_POSITION = TEXT9_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT9_CURRENT_POSITION > TEXT9_X_POSITION) {
                            TEXT9_CURRENT_POSITION = TEXT9_CURRENT_POSITION + TEXT9_VELOCITY;
                        } else {
                            TEXT9_CURRENT_POSITION = TEXT9_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT8_MOVEMENT_END && timer < TEXT10_MOVEMENT_START) {
                    if (timer < TEXT9_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT9_MOVEMENT_START && timer <= TEXT9_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT9_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture9.setAlpha(alphaLevel);
                    gTextTexture9.setAlpha(alphaLevel);
                }
                gTextOutlineTexture9.render(TEXT9_CURRENT_POSITION-3, TEXT9_Y_POSITION-3);
                gTextTexture9.render(TEXT9_CURRENT_POSITION, TEXT9_Y_POSITION);

                if (timer >= TEXT10_MOVEMENT_START) {
                    if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_LEFT) {
                        if (TEXT10_CURRENT_POSITION < TEXT10_X_POSITION) {
                            TEXT10_CURRENT_POSITION = TEXT10_CURRENT_POSITION + TEXT10_VELOCITY;
                        } else {
                            TEXT10_CURRENT_POSITION = TEXT10_X_POSITION;
                        }
                    }
                    else if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_SLIDE_RIGHT) {
                        if (TEXT10_CURRENT_POSITION > TEXT10_X_POSITION) {
                            TEXT10_CURRENT_POSITION = TEXT10_CURRENT_POSITION + TEXT10_VELOCITY;
                        } else {
                            TEXT10_CURRENT_POSITION = TEXT10_X_POSITION;
                        }
                    }
                }
                if (customTextVideo->GetappearDirection() == ICD_CUST_RADIO_APPEAR && timer > TEXT9_MOVEMENT_END) {
                    if (timer < TEXT10_MOVEMENT_START) alphaLevel = 0;
                    if (timer >= TEXT10_MOVEMENT_START && timer <= TEXT10_MOVEMENT_END) {
                        alphaLevel += 2;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT10_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextOutlineTexture10.setAlpha(alphaLevel);
                    gTextTexture10.setAlpha(alphaLevel);
                }
                gTextOutlineTexture10.render(TEXT10_CURRENT_POSITION-3, TEXT10_Y_POSITION-3);
                gTextTexture10.render(TEXT10_CURRENT_POSITION, TEXT10_Y_POSITION);
            }
            else if (video->getVideoType() == TYPE_TEMP) {
                TemperatureVideo* temperatureVideo = (TemperatureVideo*)video;

                if (timer == 0) {
                    gTextTemperatureOutline1.setAlpha(0);
                    gTextTemperature1.setAlpha(0);
                    gTextTemperatureOutline2.setAlpha(0);
                    gTextTemperature2.setAlpha(0);
                    gTextTemperatureOutline3.setAlpha(0);
                    gTextTemperature3.setAlpha(0);
                }

                if (timer < TEXT2_MOVEMENT_START) {
                    if (timer <= TEXT1_MOVEMENT_START) alphaLevel = 0;
                    else if (timer >= TEXT1_MOVEMENT_START && timer <= TEXT1_MOVEMENT_END && alphaLevel < 255) {
                        alphaLevel += 3;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT1_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextTemperatureOutline1.setAlpha(alphaLevel);
                    gTextTemperature1.setAlpha(alphaLevel);
                }
                gTextTemperatureOutline1.render(TEXT1_X_POSITION-3, TEXT1_Y_POSITION-3);
                gTextTemperature1.render(TEXT1_X_POSITION, TEXT1_Y_POSITION);

                if (timer > TEXT1_MOVEMENT_END && timer < TEXT3_MOVEMENT_START) {
                    if (timer <= TEXT2_MOVEMENT_START) alphaLevel = 0;
                    else if (timer >= TEXT2_MOVEMENT_START && timer <= TEXT2_MOVEMENT_END && alphaLevel < 255) {
                        alphaLevel += 3;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT2_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextTemperatureOutline2.setAlpha(alphaLevel);
                    gTextTemperature2.setAlpha(alphaLevel);
                }
                gTextTemperatureOutline2.render(TEXT2_X_POSITION-3, TEXT2_Y_POSITION-3);
                gTextTemperature2.render(TEXT2_X_POSITION, TEXT2_Y_POSITION);

                if (timer > TEXT2_MOVEMENT_END) {
                    if (timer <= TEXT3_MOVEMENT_START) alphaLevel = 0;
                    else if (timer >= TEXT3_MOVEMENT_START && timer <= TEXT3_MOVEMENT_END && alphaLevel < 255) {
                        alphaLevel += 3;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT3_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextTemperatureOutline3.setAlpha(alphaLevel);
                    gTextTemperature3.setAlpha(alphaLevel);
                }
                gTextTemperatureOutline3.render(TEXT3_X_POSITION-3, TEXT3_Y_POSITION-3);
                gTextTemperature3.render(TEXT3_X_POSITION, TEXT3_Y_POSITION);
            }
            else if (video->getVideoType() == TYPE_DATE) {
                DateVideo* dateVideo = (DateVideo*)video;

                if (timer == 0) {
                    gTextDateOutline1.setAlpha(0);
                    gTextDate1.setAlpha(0);
                    gTextDateOutline2.setAlpha(0);
                    gTextDate2.setAlpha(0);
                    gTextDateOutline3.setAlpha(0);
                    gTextDate3.setAlpha(0);
                }

                if (timer < TEXT2_MOVEMENT_START) {
                    if (timer <= TEXT1_MOVEMENT_START) alphaLevel = 0;
                    else if (timer >= TEXT1_MOVEMENT_START && timer <= TEXT1_MOVEMENT_END && alphaLevel < 255) {
                        alphaLevel += 3;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT1_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextDateOutline1.setAlpha(alphaLevel);
                    gTextDate1.setAlpha(alphaLevel);
                }
                gTextDateOutline1.render(TEXT1_X_POSITION-3, TEXT1_Y_POSITION-3);
                gTextDate1.render(TEXT1_X_POSITION, TEXT1_Y_POSITION);

                if (timer > TEXT1_MOVEMENT_END && timer < TEXT3_MOVEMENT_START) {
                    if (timer <= TEXT2_MOVEMENT_START) alphaLevel = 0;
                    else if (timer >= TEXT2_MOVEMENT_START && timer <= TEXT2_MOVEMENT_END && alphaLevel < 255) {
                        alphaLevel += 3;
                        if (alphaLevel > 255) alphaLevel = 255;
                    }
                    else if (timer > TEXT2_MOVEMENT_END || alphaLevel == 255) {
                        alphaLevel = 255;
                    }
                    gTextDateOutline2.setAlpha(alphaLevel);
                    gTextDate2.setAlpha(alphaLevel);
                }
                gTextDateOutline2.render(TEXT2_X_POSITION-3, TEXT2_Y_POSITION-3);
                gTextDate2.render(TEXT2_X_POSITION, TEXT2_Y_POSITION);

                if (dateVideo->Getyear() == true) {
                    if (timer > TEXT2_MOVEMENT_END) {
                        if (timer <= TEXT3_MOVEMENT_START) alphaLevel = 0;
                        else if (timer >= TEXT3_MOVEMENT_START && timer <= TEXT3_MOVEMENT_END && alphaLevel < 255) {
                            alphaLevel += 3;
                            if (alphaLevel > 255) alphaLevel = 255;
                        }
                        else if (timer > TEXT3_MOVEMENT_END || alphaLevel == 255) {
                            alphaLevel = 255;
                        }
                        gTextDateOutline3.setAlpha(alphaLevel);
                        gTextDate3.setAlpha(alphaLevel);
                    }
                    gTextDateOutline3.render(TEXT3_X_POSITION-3, TEXT3_Y_POSITION-3);
                    gTextDate3.render(TEXT3_X_POSITION, TEXT3_Y_POSITION);
                }
            }

            //Update screen
            SDL_RenderPresent( gRenderer );

            SDL_Delay(10);
            timer = (timer + 1)%TOTAL_SCENE_TIME;
        }
	}
	//Free resources and close SDL
	close();
	quitSDL();

	return 0;
}


#endif // VISUAL_LOGIC_H_INCLUDED
